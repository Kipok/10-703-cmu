import gym
import numpy as np
import tensorflow as tf
from keras.models import model_from_yaml
import matplotlib.pyplot as plt


def choose_actions(model, state, sess, argmax=False):
    probs = sess.run(model.output, {model.input: np.expand_dims(state, 0)}).squeeze()
    if argmax:
        return np.argmax(probs)
    else:
        return np.random.choice(2, 1, p=probs).squeeze()


def onehot(x, num):
    out = np.zeros((x.size, num))
    out[np.arange(x.size), x] = 1.0
    return out.squeeze()


def evaluate_model(env, model, sess, num_episodes=100, render=False, argmax=False):
    rewards = []
    max_iter = 500
    for ep_num in range(num_episodes):
        state = env.reset()
        total_reward = 0.0
        for i in range(max_iter):
            if render:
                env.render()
            action = choose_actions(model, state, sess, argmax=argmax)
            state, reward, is_term, other_info = env.step(action)
            total_reward += reward
            if is_term:
                break
        rewards.append(total_reward)
    rewards = np.array(rewards)
    return np.mean(rewards), np.min(rewards), np.max(rewards)


def get_data_from_episode(env, model, sess):
    max_iter = 500
    gamma = 0.99
    states = [env.reset()]
    actions = []
    rewards = []
    gamma_cur = 1.0
    for i in range(max_iter):
        action = choose_actions(model, states[i], sess)
        actions.append(action)
        new_state, reward, is_term, other_info = env.step(action)
        rewards.append(reward * gamma_cur)
        gamma_cur *= gamma
        if is_term:
            break
        states.append(new_state)
    return (np.array(states), onehot(np.array(actions, np.int), env.action_space.n),
            np.flip(np.cumsum(np.array(rewards[::-1])), 0))


def wrap_cartpole(env):
    """Start CartPole-v0 in a hard to recover state.

    The basic CartPole-v0 starts in easy to recover states. This means
    that the cloned model actually can execute perfectly. To see that
    the expert policy is actually better than the cloned policy, this
    function returns a modified CartPole-v0 environment. The
    environment will start closer to a failure state.

    You should see that the expert policy performs better on average
    (and with less variance) than the cloned model.

    Parameters
    ----------
    env: gym.core.Env
      The environment to modify.

    Returns
    -------
    gym.core.Env
    """
    unwrapped_env = env.unwrapped
    unwrapped_env.orig_reset = unwrapped_env._reset

    def harder_reset():
        unwrapped_env.orig_reset()
        unwrapped_env.state[0] = np.random.choice([-1.5, 1.5])
        unwrapped_env.state[1] = np.random.choice([-2., 2.])
        unwrapped_env.state[2] = np.random.choice([-.17, .17])
        return unwrapped_env.state.copy()

    unwrapped_env._reset = harder_reset

    return env


def test_cloned_policy(env, model, sess, num_episodes=50, render=False):
    total_rewards = []

    for i in range(num_episodes):
        if render:
            print('Starting episode {}'.format(i))
        total_reward = 0
        state = env.reset()
        if render:
            env.render()
            time.sleep(.1)
        is_done = False
        while not is_done:
            action = choose_actions(model, state, sess, True)
            state, reward, is_done, _ = env.step(action)
            total_reward += reward
            if render:
                env.render()
                time.sleep(.1)
        if render:
            print(
                'Total reward: {}'.format(total_reward))
        total_rewards.append(total_reward)

    print('Average total reward: {} (std: {})'.format(
        np.mean(total_rewards), np.std(total_rewards)))


def run_reinforce(num_episodes):
    sess = tf.Session()
    name = 'reinforce'
    num_actions = 2
    with open('CartPole-v0_config.yaml', 'r') as f, tf.name_scope(name):
        model = model_from_yaml(f.read())
    total_rewards = tf.placeholder(tf.float32, [None])
    actions_onehot = tf.placeholder(tf.float32, [None, num_actions])
    action_output = tf.reduce_sum(model.output * actions_onehot, axis=1)
    loss = -tf.reduce_mean(total_rewards * tf.log(action_output), axis=0)
    train_op = tf.train.AdamOptimizer(learning_rate=0.0005).minimize(loss)

    sess.run(tf.global_variables_initializer())

    env = gym.make('CartPole-v0')
    num_episodes = num_episodes
    rs, rs_mn, rs_mx, iters = [], [], [], []
    for iter_num in range(num_episodes):
        states, actions, rewards = get_data_from_episode(env, model, sess)
        cur_loss, _ = sess.run([loss, train_op], {model.input: states,
                                              actions_onehot: actions,
                                              total_rewards: rewards})
        if iter_num % 20 == 0:
            r, r_mn, r_mx = evaluate_model(env, model, sess, render=False)
            rs.append(r)
            rs_mn.append(r_mn)
            rs_mx.append(r_mx)
            iters.append(iter_num)
            print("iter# {}: {:.4f}".format(iter_num, cur_loss))
            print("evaluation: {:.2f}, {:.2f}, {:.2f}".format(r, r_mn, r_mx))

    rs = np.array(rs)
    rs_mn = np.array(rs_mn)
    rs_mx = np.array(rs_mx)

    plt.figure(figsize=(12,8))
    plt.errorbar(iters, rs, yerr=[rs - rs_mn, rs_mx - rs], fmt='--o')
    plt.title('REINFORCE learning')
    plt.xlabel('iteration')
    plt.ylabel('test reward')
    plt.show()

    hard_env = wrap_cartpole(gym.make('CartPole-v0'))
    test_cloned_policy(env, model, sess)
    test_cloned_policy(hard_env, model, sess)

