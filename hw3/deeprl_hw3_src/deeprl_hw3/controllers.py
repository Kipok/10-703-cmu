"""LQR, iLQR and MPC."""

import numpy as np
from scipy.linalg import solve_continuous_are
import matplotlib.pyplot as plt


def simulate_dynamics(env, x, u, dt=1e-5):
    env.state = x.copy()
    x_new, _, _, _ = env._step(u, dt)
    return (x_new - x) / dt


def approximate_B(env, x, u, delta=1e-5, dt=1e-5):
    B = np.zeros((x.shape[0], u.shape[0]))
    for j in range(u.shape[0]):
        u[j] += delta
        out_top = simulate_dynamics(env, x, u, dt)
        u[j] -= 2.0 * delta
        out_bottom = simulate_dynamics(env, x, u, dt)
        u[j] += delta
        B[:,j] = (out_top - out_bottom) / (2 * delta)
    return B


def approximate_A(env, x, u, delta=1e-5, dt=1e-5):
    A = np.zeros((x.shape[0], x.shape[0]))
    for j in range(x.shape[0]):
        x[j] += delta
        out_top = simulate_dynamics(env, x, u, dt)
        x[j] -= 2.0 * delta
        out_bottom = simulate_dynamics(env, x, u, dt)
        x[j] += delta
        A[:,j] = (out_top - out_bottom) / (2 * delta)
    return A


def solve_lqr(env, sim_env, max_iter=500, title='LQR'):
    sim_env.reset()
    x_cur = env.reset()
    u_cur = np.zeros(env.action_space.shape)
    x_goal = np.hstack((env.goal_q, env.goal_dq))
    cum_reward = 0.0
    xs = [x_cur]
    us = [u_cur]
    for iter_num in range(max_iter):
        env.render()
        cur_A = approximate_A(sim_env, x_cur, u_cur)
        # this is a hack due to problem described in @495 post on 
        # piazza which improves conditioning of matrix A
        cur_A += np.eye(cur_A.shape[0]) * 1e-7
        cur_B = approximate_B(sim_env, x_cur, u_cur)
        P = solve_continuous_are(cur_A, cur_B, env.Q, env.R)
        K = np.linalg.inv(env.R).dot(cur_B.T.dot(P))
        u_cur = -K.dot(x_cur - x_goal)
        u_cur = np.clip(u_cur, env.action_space.low,
                        env.action_space.high)
        x_cur, reward, is_term, other_info = env.step(u_cur)
        cum_reward += reward
        xs.append(x_cur)
        us.append(u_cur)
        if is_term:
            print("Environment solved")
            break

    print("Cumulative reward: {:.04f}, total steps: {:.2f}".format(cum_reward, iter_num))
    xs = np.array(xs)
    us = np.array(us)
    fig, axes = plt.subplots(6, 1, figsize=(15, 15))
    axes[0].set_title(title + " [cum_reward = {:.2f}, total_steps = {}]"\
                      .format(cum_reward, iter_num))
    axes[0].plot(np.arange(xs.shape[0]), xs[:,0])
    axes[0].set_ylabel('q[0]')
    axes[1].plot(np.arange(xs.shape[0]), xs[:,1])
    axes[1].set_ylabel('q[1]')
    axes[2].plot(np.arange(xs.shape[0]), xs[:,2])
    axes[2].set_ylabel('dq[0]')
    axes[3].plot(np.arange(xs.shape[0]), xs[:,3])
    axes[3].set_ylabel('dq[1]')
    axes[4].plot(np.arange(us.shape[0]), us[:,0])
    axes[4].set_ylabel('u[0]')
    axes[5].plot(np.arange(us.shape[0]), us[:,1])
    axes[5].set_ylabel('u[1]')
    axes[5].set_xlabel('iter num')
    plt.show()
    return xs, us

