"""LQR, iLQR and MPC."""

from deeprl_hw3.controllers import approximate_A, approximate_B
import numpy as np
from scipy.linalg import block_diag
import matplotlib.pyplot as plt


def simulate_dynamics(env, x, u, dt=1e-3):
    env.state = x.copy()
    x_new, _, _, _ = env._step(u, dt)
    return x_new


def approximate_B(env, x, u, delta=1e-5, dt=1e-3):
    B = np.zeros((x.shape[0], u.shape[0]))
    for j in range(u.shape[0]):
        u[j] += delta
        out_top = simulate_dynamics(env, x, u, dt)
        u[j] -= 2.0 * delta
        out_bottom = simulate_dynamics(env, x, u, dt)
        u[j] += delta
        B[:,j] = (out_top - out_bottom) / (2 * delta)
    return B


def approximate_A(env, x, u, delta=1e-5, dt=1e-3):
    A = np.zeros((x.shape[0], x.shape[0]))
    for j in range(x.shape[0]):
        x[j] += delta
        out_top = simulate_dynamics(env, x, u, dt)
        x[j] -= 2.0 * delta
        out_bottom = simulate_dynamics(env, x, u, dt)
        x[j] += delta
        A[:,j] = (out_top - out_bottom) / (2 * delta)
    return A


COEF_X = 1e8
COEF_U = 1


def cost_inter(x, u):
    C = 2.0 * COEF_U * block_diag(0 * np.eye(x.shape[0]), np.eye(u.shape[0]))
    c = C.dot(np.hstack((x, u)))
    return C, c


def cost_final(env, x, u):
    C = 2.0 * COEF_X * block_diag(np.eye(x.shape[0]), 0 * np.eye(u.shape[0]))
    c = -1.0 * np.hstack((env.goal, np.zeros(2))).dot(C) + C.dot(np.hstack((x, u)))
    return C, c


def calc_cost_final(x, u, xg):
    return COEF_X * (x - xg).T.dot(x - xg)


def calc_cost_inter(x, u):
    return COEF_U * u.T.dot(u)


def ilqr_backward_pass(env, sim_env, tN, us, xs):
    bd = env.observation_space.shape[0]

    V = np.zeros((4, 4))
    v = np.zeros(4)

    Ks = [None] * tN
    ks = [None] * tN

    for t in range(tN - 1, -1, -1):
        A = approximate_A(sim_env, xs[t], us[t])
        B = approximate_B(sim_env, xs[t], us[t])
        F = np.hstack((A, B))

        if t == tN - 1:
            C, c = cost_final(env, xs[t], us[t])
        else:
            C, c = cost_inter(xs[t], us[t])

        Q = C + F.T.dot(V.dot(F))
        q = c + F.T.dot(v)

        K = -np.linalg.inv(Q[bd:, bd:] + 1 * np.eye(2)).dot(Q[bd:, :bd])
        k = -np.linalg.inv(Q[bd:, bd:] + 1 * np.eye(2)).dot(q[bd:])

        Ks[t] = K
        ks[t] = k

        V = Q[:bd, :bd] + Q[:bd, bd:].dot(K) + K.T.dot(Q[bd:, :bd]) + \
            K.T.dot(Q[bd:, bd:].dot(K))
        v = q[:bd] + Q[:bd, bd:].dot(k) + K.T.dot(q[bd:]) + K.T.dot(Q[bd:, bd:].dot(k))

    return Ks, ks

def ilqr_forward_pass(env, Ks, ks, us, xs, tN, alpha=0.5):
    us_new = [None] * tN
    xs_new = [None] * tN
    xs_new[0] = env.reset()
    cum_reward = 0
    cost = 0

    for t in range(tN):
        us_new[t] = us[t] + Ks[t].dot(xs_new[t] - xs[t]) + alpha * ks[t]
        if t == tN - 1:
            cost += calc_cost_final(xs_new[t] - xs[t], us_new[t] - us[t], env.goal)
        else:
            cost += calc_cost_inter(xs_new[t] - xs[t], us_new[t] - us[t])
            xs_new[t + 1], reward, is_term, other_info = env.step(us_new[t])
        cum_reward += reward

    return us_new, xs_new, cum_reward, cost, is_term

def run_ilqr(env, sim_env, tN, start_alpha, title):
    us_cur = np.zeros((tN,) + env.action_space.shape)
    xs_cur = np.zeros((tN,) + env.observation_space.shape)
    xs_cur[0] = env.reset()
    for i in range(tN - 1):
        xs_cur[i + 1], _, _, _ = env.step(us_cur[i])

    cost_old = np.inf
    num_iter = 1000
    alpha = start_alpha
    tol = 1e-5
    iter_cost = []

    for i in range(num_iter):
        Ks, ks = ilqr_backward_pass(env, sim_env, tN, us_cur, xs_cur)
        us_cur, xs_cur, reward, cost, is_term = \
                ilqr_forward_pass(env, Ks, ks, us_cur, xs_cur, tN, alpha)
        if is_term:
            print("Environment solved")
            break
        if np.abs(cost - cost_old) < tol:
            print("Converged")
            break
        if cost > cost_old:
            while cost > cost_old:
                if alpha < 0.00001:
                    break
                alpha /= 2.0
                us_cur, xs_cur, reward, cost, is_term = \
                        ilqr_forward_pass(env, Ks, ks, us_cur, xs_cur, tN, alpha)
        else:
            alpha *= 2.0
            alpha = min(alpha, start_alpha)

        cost_old = cost
        iter_cost.append(cost)

        if i % 100 == 0:
            print("iter #{}: r={:.2f}, c={:.2f}".format(i, reward, cost))
            env.reset()
            for t in range(tN - 1):
                env.render()
                x_cur, r, is_term, other_info = env.step(us_cur[t])

    env.reset()
    for t in range(tN - 1):
        env.render()
        x_cur, r, is_term, other_info = env.step(us_cur[t])
    env.close()

    print("Final reward: {:.02f}, total cost: {:.02f}".format(reward, cost))
    xs = np.array(xs_cur)
    us = np.array(us_cur)
    fig, axes = plt.subplots(6, 1, figsize=(15, 15))
    axes[0].set_title(title + " [final_reward = {:.2f}, final_cost = {:.2f}]"\
                      .format(reward, cost))
    axes[0].plot(np.arange(xs.shape[0]), xs[:,0])
    axes[0].set_ylabel('q[0]')
    axes[1].plot(np.arange(xs.shape[0]), xs[:,1])
    axes[1].set_ylabel('q[1]')
    axes[2].plot(np.arange(xs.shape[0]), xs[:,2])
    axes[2].set_ylabel('dq[0]')
    axes[3].plot(np.arange(xs.shape[0]), xs[:,3])
    axes[3].set_ylabel('dq[1]')
    axes[4].plot(np.arange(us.shape[0]), us[:,0])
    axes[4].set_ylabel('u[0]')
    axes[5].plot(np.arange(us.shape[0]), us[:,1])
    axes[5].set_ylabel('u[1]')
    axes[5].set_xlabel('iter num')
    plt.show()

    plt.title("Cost by iterations")
    plt.plot(np.arange(len(iter_cost)), iter_cost)
    plt.xlabel('Iteration number')
    plt.ylabel('Cost')

