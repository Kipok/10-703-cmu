"""Functions for imitation learning."""
from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

from keras.models import model_from_yaml
from keras.optimizers import Adam
from keras.losses import binary_crossentropy
import tensorflow as tf

import numpy as np
import time
import gym

import matplotlib.pyplot as plt


def load_model(model_config_path, model_weights_path=None):
    """Load a saved model.

    Parameters
    ----------
    model_config_path: str
      The path to the model configuration yaml file. We have provided
      you this file for problems 2 and 3.
    model_weights_path: str, optional
      If specified, will load keras weights from hdf5 file.

    Returns
    -------
    keras.models.Model
    """
    with open(model_config_path, 'r') as f:
        model = model_from_yaml(f.read())

    if model_weights_path is not None:
        model.load_weights(model_weights_path)

    return model


def generate_expert_training_data(expert, env, num_episodes=100, render=False):
    """Generate training dataset.

    Parameters
    ----------
    expert: keras.models.Model
      Model with expert weights.
    env: gym.core.Env
      The gym environment associated with this expert.
    num_episodes: int, optional
      How many expert episodes should be run.
    render: bool, optional
      If present, render the environment, and put a slight pause after
      each action.

    Returns
    -------
    expert_dataset: ndarray(states), ndarray(actions)
      Returns two lists. The first contains all of the states. The
      second contains a one-hot encoding of all of the actions chosen
      by the expert for those states.
    """
    max_iter = 500

    def onehot(x, num):
        out = np.zeros((x.size, num))
        out[np.arange(x.size), x] = 1.0
        return out.squeeze()

    states = []
    actions = []

    for i in range(num_episodes):
        states.append(env.reset())
        for j in range(max_iter):
            if render:
                env.render()
            action = np.argmax(expert.predict(np.expand_dims(states[-1], 0), batch_size=1))
            next_state, reward, is_term, other_info = env.step(action)
            actions.append(onehot(action, env.action_space.n))
            if is_term:
                break
            states.append(next_state)

    states = np.array(states)
    actions = np.array(actions)
    return states, actions


def train_policy(env, expert, train_states, train_actions, name, num_epochs=50):
    with open('CartPole-v0_config.yaml', 'r') as f, tf.name_scope(name):
        model = model_from_yaml(f.read())
        model.compile(optimizer=Adam(lr=0.0001), loss=binary_crossentropy, metrics=['accuracy'])
    model.fit(train_states, train_actions, epochs=num_epochs)
    return model


def test_cloned_policy(env, cloned_policy, num_episodes=50, render=False):
    """Run cloned policy and collect statistics on performance.

    Will print the rewards for each episode and the mean/std of all
    the episode rewards.

    Parameters
    ----------
    env: gym.core.Env
      The CartPole-v0 instance.
    cloned_policy: keras.models.Model
      The model to run on the environment.
    num_episodes: int, optional
      Number of test episodes to average over.
    render: bool, optional
      If true, render the test episodes. This will add a small delay
      after each action.
    """
    total_rewards = []

    for i in range(num_episodes):
        if render:
            print('Starting episode {}'.format(i))
        total_reward = 0
        state = env.reset()
        if render:
            env.render()
            time.sleep(.1)
        is_done = False
        while not is_done:
            action = np.argmax(
                cloned_policy.predict_on_batch(state[np.newaxis, ...])[0])
            state, reward, is_done, _ = env.step(action)
            total_reward += reward
            if render:
                env.render()
                time.sleep(.1)
        if render:
            print(
                'Total reward: {}'.format(total_reward))
        total_rewards.append(total_reward)

    print('Average total reward: {} (std: {})'.format(
        np.mean(total_rewards), np.std(total_rewards)))


def wrap_cartpole(env):
    """Start CartPole-v0 in a hard to recover state.

    The basic CartPole-v0 starts in easy to recover states. This means
    that the cloned model actually can execute perfectly. To see that
    the expert policy is actually better than the cloned policy, this
    function returns a modified CartPole-v0 environment. The
    environment will start closer to a failure state.

    You should see that the expert policy performs better on average
    (and with less variance) than the cloned model.

    Parameters
    ----------
    env: gym.core.Env
      The environment to modify.

    Returns
    -------
    gym.core.Env
    """
    unwrapped_env = env.unwrapped
    unwrapped_env.orig_reset = unwrapped_env._reset

    def harder_reset():
        unwrapped_env.orig_reset()
        unwrapped_env.state[0] = np.random.choice([-1.5, 1.5])
        unwrapped_env.state[1] = np.random.choice([-2., 2.])
        unwrapped_env.state[2] = np.random.choice([-.17, .17])
        return unwrapped_env.state.copy()

    unwrapped_env._reset = harder_reset

    return env


def run_exps(num_eps=50):
    env = gym.make('CartPole-v0')
    expert = load_model('CartPole-v0_config.yaml', 'CartPole-v0_weights.h5f')
    wr_env = wrap_cartpole(gym.make('CartPole-v0'))
    print("Evaluating expert:")
    test_cloned_policy(wr_env, expert)

    for nm in [1, 10, 50, 100]:
        print("Data {}:".format(nm))
        states, actions = generate_expert_training_data(expert, env, nm)
        model = train_policy(env, expert, states, actions, 'model{}'.format(nm), num_eps)
        print("Running on env")
        test_cloned_policy(env, model)
        print("Running on wr_env")
        test_cloned_policy(wr_env, model)


def gen_dagger_training_data(env, expert, model, num_episodes=5):
    max_iter = 500

    def onehot(x, num):
        out = np.zeros((x.size, num))
        out[np.arange(x.size), x] = 1.0
        return out.squeeze()

    states = []
    actions = []

    for i in range(num_episodes):
        states.append(env.reset())
        for j in range(max_iter):
            action = np.argmax(model.predict(np.expand_dims(states[-1], 0), batch_size=1))
            action_exp = np.argmax(expert.predict(np.expand_dims(states[-1], 0), batch_size=1))
            next_state, reward, is_term, other_info = env.step(action)
            actions.append(onehot(action_exp, env.action_space.n))
            if is_term:
                break
            states.append(next_state)

    states = np.array(states)
    actions = np.array(actions)
    return states, actions


def evaluate_model(env, model, num_episodes=100, render=False):
    rewards = []
    max_iter = 500
    for ep_num in range(num_episodes):
        state = env.reset()
        total_reward = 0.0
        for i in range(max_iter):
            if render:
                env.render()
            action = np.argmax(model.predict(np.expand_dims(state, 0), batch_size=1))
            state, reward, is_term, other_info = env.step(action)
            total_reward += reward
            if is_term:
                break
        rewards.append(total_reward)
    rewards = np.array(rewards)
    return np.mean(rewards), np.min(rewards), np.max(rewards)


def run_dagger(num_dagger_iters=20):
    env = gym.make('CartPole-v0')
    wr_env = wrap_cartpole(gym.make('CartPole-v0'))

    expert = load_model('CartPole-v0_config.yaml', 'CartPole-v0_weights.h5f')
    with open('CartPole-v0_config.yaml', 'r') as f, tf.name_scope('model'):
        model = model_from_yaml(f.read())
        model.compile(optimizer=Adam(lr=0.0001), loss=binary_crossentropy, metrics=['accuracy'])

    num_epochs = 50
    train_states, train_actions = gen_dagger_training_data(env, expert, model)
    rws, rw_mns, rw_mxs = [], [], []
    h_rws, h_rw_mns, h_rw_mxs = [], [], []

    for i in range(num_dagger_iters):
        print("iter #{}".format(i))
        model.fit(train_states, train_actions, epochs=num_epochs, verbose=0)
        rw, rw_mn, rw_mx = evaluate_model(env, model)
        print("simple:", rw, rw_mn, rw_mx)
        rws.append(rw)
        rw_mns.append(rw_mn)
        rw_mxs.append(rw_mx)

        rw, rw_mn, rw_mx = evaluate_model(wr_env, model)
        print("hard:", rw, rw_mn, rw_mx)
        h_rws.append(rw)
        h_rw_mns.append(rw_mn)
        h_rw_mxs.append(rw_mx)

        states, actions = gen_dagger_training_data(env, expert, model)
        train_states = np.vstack((train_states, states))
        train_actions = np.vstack((train_actions, actions))

    test_cloned_policy(env, model)
    test_cloned_policy(wr_env, model)

    rs = np.array(rws)
    rs_mn = np.array(rw_mns)
    rs_mx = np.array(rw_mxs)

    plt.figure(figsize=(12,8))
    plt.errorbar(np.arange(rs.shape[0]), rs, yerr=[rs - rs_mn, rs_mx - rs], fmt='--o')
    plt.title('DAGGER simple env')
    plt.xlabel('iteration')
    plt.ylabel('test reward')
    plt.show()

    rs = np.array(h_rws)
    rs_mn = np.array(h_rw_mns)
    rs_mx = np.array(h_rw_mxs)

    plt.figure(figsize=(12,8))
    plt.errorbar(np.arange(rs.shape[0]), rs, yerr=[rs - rs_mn, rs_mx - rs], fmt='--o')
    plt.title('DAGGER hard env')
    plt.xlabel('iteration')
    plt.ylabel('test reward')
    plt.show()

