\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}

% \usepackage{nips_2016}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{amsmath}
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{enumerate}
\usepackage{fancyvrb}
\usepackage{hyperref}
\usepackage{placeins}
\usepackage{tikz}
\usepackage{tikzsymbols}
\usepackage{todonotes}
\usepackage{adjustbox}

\title{Homework 3}

\author{
  Igor Gitman, igitman
}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Nm}{\mathcal{N}}
\newcommand{\Ll}{\mathcal{L}}

% brackets
\newcommand{\inner}[2]{\left\langle#1,#2 \right\rangle}
\newcommand{\rbr}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1\right\}}
\newcommand{\nbr}[1]{\left\|#1\right\|}
\newcommand{\abr}[1]{\left|#1\right|}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}

\maketitle
\section*{LQR}
For this problem I used infinite-horizon continuous closed-loop version of LQR.
\subsection*{1}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{lqr1.png}
    \end{center}
\newpage
\subsection*{2}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{lqr2.png}
    \end{center}
\subsection*{3}
    We can see that when we have limited torques the convergence is much slower, which is logical since the agent has less flexibility in controlling
    the arm. It is also visible from the fact that in many iterations actions are set to boundary values when the agent essentially tries to stop the
    momentum and slow down the arm as fast as possible.
\newpage
\subsection*{4}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{lqr3.png}
    \end{center}
\newpage
\subsection*{5}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{lqr4.png}
    \end{center}
\subsection*{6}
    We can see that higher cost for controls makes performance on TwoLinkArm-v1 worse. This is because actions with big magnitude will lead to the
    high internal cost, while they might be optimal for time minimization.

    However for the limited torques we observe the reverse effect which might be because when we solve the differential equations it is assumed that
    actions are unlimited. However when agent applies them to the environment they are actually clipped and thus we don't follow the computed
    trajectory. When we increase cost for big actions larger part of the trajectory that is computed falls under the clipped range which makes the
    convergence faster.

\newpage
\section*{iLQR}
\subsection*{1}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{ilqr1.png}
        \includegraphics[width=0.6\textwidth]{ilqr2.png}
    \end{center}
\newpage
\subsection*{2}
    Since the cost for iLQR is defined outside the environment and I initialize controls 
    deterministically with zeros, all the plots look exactly the same, but final reward is 
    different: $-2489.69$.
\subsection*{3}
    In general LQR might be better in online settings or in the settings when the number of iterations doesn't matter and the only goal is to get to
    the final state eventually. However in iLQR we can specify the number of iterations and then optimize the trajectory constrained on that number.
    Also iLQR in general has better approximation of the dynamics of the environment since it is using correct linear approximation for all the
    points, while LQR approximation is only correct for the initial point. But iLQR in general requires more tuning and more computationally expensive
    to run then LQR.

\subsection*{Speed up iLQR}
    To speed up iLQR I used the following techniques. First, I changed the loss function to be $10^8\nbr{x_{\text{tN}} - x^*}$. And second, I added a
    simple line search for $\alpha$ where if cost is increasing during the forward pass, I run it again with $\alpha / 2$. If cost has decreased, I
    double the $\alpha$ to encourage big steps.


\section*{Behavior Cloning}
    Each model was trained with Adam optimizer (learning rate $= 0.0001$ and other default parameters). Here are the results:

    \begin{tabular}{|l|c|c|c|c|c|}
    \hline
    {} & expert & 1 episode & 10 episodes & 50 episodes & 100 episodes \\
    \hline
    accuracy & - & 0.84 & 0.88 & 0.92 & 0.97 \\
    \hline
    loss & - & 0.64 & 0.27 & 0.19 & 0.08 \\
    \hline
    simple reward & $200\pm 0$ & $112\pm 30$ & $185\pm 24$ & $200\pm 0$ & $200\pm 0$ \\
    \hline
    hard reward & $71\pm44$ & $12\pm 9$ & $36\pm 45$ & $60\pm 64$ & $62\pm 72$ \\
    \hline
    \end{tabular}
\subsection*{2}
    We can see that as expected the more training data was used, the better is the performance of the model.
\subsection*{3}
    We can see that on hard environment we have the same trend as on the simple one (more training data, better results). Also the variance is
    somewhat bigger for trained models then for expert model.

\section*{DAGGER}
For DAGGER the following hyperparameters were used. Adam optimizer for training with learning rate $= 0.0001$, training on each iteration was done for
$50$ epochs. There were $20$ global iterations, on each iteration only current policy was used to generate the trajectories ($\beta_i=0$) and the total
length of the trajectories was $1000$.
\newpage
\subsection*{1}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{dagger1.png}
        \includegraphics[width=1.0\textwidth]{dagger2.png}
    \end{center}
\subsection*{2}
    The algorithm training with DAGGER achieves average reward of $200\pm 0$ on a simple environment and reward of $65\pm 64$ on the hard environment.
    This shows that it performs on the same level as top models from behavior cloning algorithms and the expert model. The variance for the hard
    environment is still large comparing to the expert model, but it is not clear why DAGGER should help for this setup since the agent doesn't
    experience difficult states when training on a simple environment and thus can't improve the performance for them.

\section*{REINFORCE}
\subsection*{1}
    \begin{center}
        \includegraphics[width=1.0\textwidth]{reinforce.png}
    \end{center}
    
\subsection*{2}
    The model trained with REINFORCE can also achieve average reward of 200 on the CartPole environment ans average reward of 74.82 with standard
    deviation of 76 on the hard version of that environment. So it is working on the same level as expert model or models
    trained with behavior cloning. However training is somewhat unstable since stochastic policy is used to select actions. This can be solved by
    starting to follow deterministic argmax policy on the latter iterations of training and use it for evaluation.
\end{document}
