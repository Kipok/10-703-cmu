import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from policy import GreedyEpsilonPolicy
from memory import ReplayMemory
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from memory import ReplayMemory
import matplotlib
import time


class SupervisedAgent:
    def __init__(self, num_actions, state_size, gamma=0.99, agent_fixed=None):
        self.num_actions = num_actions
        self.state_size = state_size
        self.gamma = gamma
        self.rgrs = []
        self.last_vals = np.zeros((500*500, 3))
        self.times = []
        self.norms = []
        self.agent_fixed = agent_fixed

    def _onehot(self, x, num):
        out = np.zeros((x.size, num))
        out[np.arange(x.size), x] = 1.0
        return out

    def get_action_values(self, states):
        # TODO: not efficeint since mostly need it 
        #       for particular actions only
        output = np.zeros((states.shape[0], self.num_actions))
        for rgr in self.rgrs:
            for action_i in range(self.num_actions):
                output[:, action_i] += rgr[action_i].predict(states)
        return output

    def get_action_values_fixed(self, states):
        assert(self.agent_fixed is not None)
        return self.agent_fixed.get_action_values(states)

    def select_action(self, state, eps):
        if np.random.rand() < eps:
            return np.random.randint(self.num_actions)
        else:
            q_values = self.get_action_values_fixed(state[np.newaxis,:])
            return np.argmax(q_values, axis=1).squeeze()

    def output_monitor(self):
        matplotlib.rcParams.update({'font.size': 36})
        sz = 500
        x = np.linspace(-1.2, 0.6, sz)
        y = np.linspace(-0.07, 0.07, sz)
        xx, yy = np.meshgrid(x, y)
        states = np.stack((xx.reshape(-1), yy.reshape(-1)), 1)
        action_values = self.get_action_values(states)
        vmin = np.min(action_values)
        vmax = np.max(action_values)

        fig, axes = plt.subplots(figsize=(34, 7), nrows=1, ncols=3, sharey=True)
        for t in range(self.num_actions):
            axes[t].xaxis.set_tick_params(width=2, length=8)
            if t == 0:
                axes[t].yaxis.set_tick_params(width=2, length=8)
            if t == 0:
                axes[t].set_title("Go to the left".format(t), y=1.05)
            if t == 1:
                axes[t].set_title("Do nothing".format(t), y=1.05)
            if t == 2:
                axes[t].set_title("Go to the right".format(t), y=1.05)

            mp = axes[t].imshow(action_values[:, t].reshape(sz, sz),
                                extent=(-1.2, 0.6, 0.7, -0.7),
                                interpolation='nearest', vmin=vmin-0.1,
                                vmax=vmax+0.1)
            axes[t].set_xlabel('position')
            axes[0].set_ylabel('velosity * 10')
        fig.subplots_adjust(right=0.8)
        cbar_ax = fig.add_axes([0.85, -0.01, 0.04, 1.0])
        cbar_ax.xaxis.set_tick_params(width=2, length=8)
        cbar_ax.yaxis.set_tick_params(width=2, length=8)
        fig.colorbar(mp, cax=cbar_ax)
        plt.show()
        self.norms.append(np.linalg.norm(self.last_vals - action_values))
        self.last_vals = action_values.copy()

    def create_rgr(self):
        return RandomForestRegressor(n_estimators=100, min_samples_split=10, n_jobs=6)

    def update_network(self, memory, hz):
        samples = memory.sample(memory.memory_size, hz)
        if hz > 0:
            e_q_s_a = self.get_action_values(
                samples[2]
            )[np.arange(memory.memory_size), samples[3]]
            target = self.gamma ** hz * e_q_s_a * samples[4]
        else:
            target = samples[2]
        # aggregating the same states
        a_train_X = []
        a_train_y = []

        for a_idx in range(self.num_actions):
            cur_states = samples[0][samples[1].squeeze() == a_idx]
            ind = np.lexsort(cur_states.T)
            cur_states = cur_states[ind]
            cur_target = target[samples[1].squeeze() == a_idx][ind]

            train_X = []
            train_y = []
            train_X.append(cur_states[0].copy())
            train_y.append(cur_target[0].copy())
            cur_num = 1
            for idx in range(1, cur_states.shape[0]):
                if np.allclose(cur_states[idx], cur_states[idx - 1]):
                    train_y[-1] += cur_target[idx]
                    cur_num += 1
                else:
                    train_y[-1] /= cur_num
                    train_y.append(cur_target[idx].copy())
                    train_X.append(cur_states[idx].copy())
                    cur_num = 1
            train_y[-1] /= cur_num
            a_train_X.append(np.array(train_X))
            a_train_y.append(np.array(train_y))
        print(a_train_y[0].min(), a_train_y[1].min(), a_train_y[2].min())
        print(a_train_y[0].max(), a_train_y[1].max(), a_train_y[2].max())

        rgr = []
        for a_idx in range(self.num_actions):
            rgr.append(self.create_rgr())
            rgr[-1].fit(a_train_X[a_idx], a_train_y[a_idx])
        self.rgrs.append(rgr)

    def fit(self, env, iters=[1000, 1000, 1000, 1000], hz_list=[0, 1, 2, 4], eps=1.0):
        for iter_num, hz in zip(iters, hz_list):
            self.times.append(time.time())
            # burning in
            memory = ReplayMemory(iter_num, (self.state_size,))
            state = env.reset()
            for i in range(memory.memory_size):
                action = self.select_action(state, eps)
                next_state, reward, is_terminal, other_info = env.step(action)
                memory.append(state, action, reward, is_terminal)
                if is_terminal:
                    state = env.reset()
                else:
                    state = next_state
            print("Finished burning in")

            print("Horizon: {}".format(hz))
            self.update_network(memory, hz)
            self.times[-1] = time.time() - self.times[-1]
            print("Generating visual image")
            self.output_monitor()


class SupervisedAgentSequential(SupervisedAgent):
    def get_action_values(self, states):
        output = np.zeros((states.shape[0], self.num_actions))
        rgr = self.rgrs[-1]
        for action_i in range(self.num_actions):
            output[:, action_i] += rgr[action_i].predict(states)
        return output

    def update_network(self, memory, hz):
        samples = memory.sample(memory.memory_size, int(hz >= 1))
        if hz > 0:
            e_q_s_a = self.get_action_values(
                samples[2]
            )[np.arange(memory.memory_size), samples[3]]
            target = (samples[5] + self.gamma ** hz * e_q_s_a) * samples[4]
        else:
            target = samples[2]
        # aggregating the same states
        a_train_X = []
        a_train_y = []

        for a_idx in range(self.num_actions):
            cur_states = samples[0][samples[1].squeeze() == a_idx]
            ind = np.lexsort(cur_states.T)
            cur_states = cur_states[ind]
            cur_target = target[samples[1].squeeze() == a_idx][ind]

            train_X = []
            train_y = []
            train_X.append(cur_states[0].copy())
            train_y.append(cur_target[0].copy())
            cur_num = 1
            for idx in range(1, cur_states.shape[0]):
                if np.allclose(cur_states[idx], cur_states[idx - 1]):
                    train_y[-1] += cur_target[idx]
                    cur_num += 1
                else:
                    train_y[-1] /= cur_num
                    train_y.append(cur_target[idx].copy())
                    train_X.append(cur_states[idx].copy())
                    cur_num = 1
            train_y[-1] /= cur_num
            a_train_X.append(np.array(train_X))
            a_train_y.append(np.array(train_y))
        print(a_train_y[0].min(), a_train_y[1].min(), a_train_y[2].min())
        print(a_train_y[0].max(), a_train_y[1].max(), a_train_y[2].max())

        rgr = []
        for a_idx in range(self.num_actions):
            rgr.append(self.create_rgr())
            rgr[-1].fit(a_train_X[a_idx], a_train_y[a_idx])
        if hz > 0:
            self.rgrs.pop(0)
        self.rgrs.append(rgr)


class Agent:
    def __init__(self, num_actions=3, state_size=2, gamma=0.99):
        self.num_actions = num_actions
        self.state_size = state_size
        self.gamma = gamma

    def get_model(self, name):
        with tf.name_scope(name):
            model = Sequential()
            # TODO: get bias back!!
            model.add(Dense(self.num_actions, use_bias=False, input_shape=(self.state_size,)))
        return model, model.input, model.output

    def compile(self, sess):
        self.q_model, self.q_input, self.q_output = self.get_model('q_model')
        self.target_model, self.target_input, self.target_output = self.get_model('target_model')

        self.target = tf.placeholder(tf.float32, [None], name='target')
        self.actions_onehot = tf.placeholder(tf.float32,
                                             [None, self.num_actions], name='actions_onehot')
        with tf.name_scope('choose_q_action'):
            self.q_action_val = tf.reduce_sum(self.q_output * self.actions_onehot,
                                              axis=1)
        with tf.name_scope('reduce_to_loss'):
            self.loss = tf.reduce_mean((self.q_action_val - self.target) ** 2)

        self.q_vars = [var for var in tf.trainable_variables()
                       if var.name.startswith('q_model')]
        self.target_vars = [var for var in tf.trainable_variables()
                            if var.name.startswith('target_model')]
        self.train_op = tf.train.AdamOptimizer(0.005).minimize(self.loss, var_list=self.q_vars)

        with tf.name_scope('update_target'):
            self.target_update_ops = [
                tf.assign(w1, w2) for w1, w2 in
                zip(self.target_vars, self.q_vars)
            ]
        with tf.name_scope('update_adam'):
            adam_vars = [var for var in tf.global_variables() if 'Adam' in var.name]
            self.adam_reset = tf.variables_initializer(adam_vars)

        self.sess = sess
        self.sess.run(tf.global_variables_initializer())

    def get_action_values(self, state, use_target):
        prep_state = state

        if use_target:
            return self.sess.run(self.target_output, {self.target_input: prep_state})
        else:
            return self.sess.run(self.q_output, {self.q_input: prep_state})

    def select_action(self, policy, target_action_values, state):
        return policy.select_action(target_action_values)

    def _onehot(self, x, num):
        out = np.zeros((x.size, num))
        out[np.arange(x.size), x] = 1.0
        return out

    def update_network(self, memory, batch_size, hz):
        samples = memory.sample(batch_size, hz)
        if hz > 0:
            q_s_a = self.get_action_values(samples[0],
                                           True)[np.arange(batch_size), samples[1]]
            e_q_s_a = np.max(self.get_action_values(samples[2], True), axis=1)
            target = q_s_a + self.gamma ** hz * e_q_s_a
        else:
            target = samples[2]

        feed_dict = {
            self.q_input: samples[0],
            self.actions_onehot: self._onehot(samples[1], self.num_actions),
            self.target: target,
        }
        _ = self.sess.run(self.train_op, feed_dict)

    def fit(self, env, policy, memory, hz_list=[0, 1, 2, 4],
            num_iters=[1000, 1000, 1000, 1000], num_burn_in=1000, monitor_freq=10000,
            batch_size=1):

        state = self._onehot(np.array([env.reset()]), self.state_size)
        for i in range(num_burn_in):
            target_action_values = self.get_action_values(state, True)
            action = self.select_action(policy, target_action_values, state)
            next_state, reward, is_terminal, other_info = env.step(action)
            memory.append(state, action, reward, is_terminal)

            if is_terminal:
                state = self._onehot(np.array([env.reset()]), self.state_size)
            else:
                state = self._onehot(np.array([next_state]), self.state_size)
        print("Finished burning in")

        for hz, num_it in zip(hz_list, num_iters):
            print("Horizon: {}".format(hz))
            self.sess.run(self.target_update_ops)
            # TODO: maybe delete this?
            self.sess.run(self.adam_reset)

            for i in range(num_it):
                target_action_values = self.get_action_values(state, True)
                action = self.select_action(policy, target_action_values, state)
                next_state, reward, is_terminal, other_info = env.step(action)
                memory.append(state, action, reward, is_terminal)

                if is_terminal:
                    state = self._onehot(np.array([env.reset()]), self.state_size)
                else:
                    state = self._onehot(np.array([next_state]), self.state_size)

                self.update_network(memory, batch_size, hz)

                if (i > 0 and i % monitor_freq == 0) or i == num_it - 1:
                    print("iter #{}".format(i))
                #    w, b = self.sess.run(self.q_vars)
                #    for t in range(self.num_actions):
                #        print("For action {}: ".format(t) +
                #              ",".join(["w{}={:.4f}".format(j, w.T[t, j]) for j in range(self.state_size)]) +
                #              "b={:.4f}".format(b[t]))
                    states = self._onehot(np.arange(4*4), self.state_size)
                    action_values = self.get_action_values(states, False)
                    vmin = np.min(action_values)
                    vmax = np.max(action_values)

                    fig, axes = plt.subplots(figsize=(15, 7), nrows=1, ncols=self.num_actions)
                    for t in range(self.num_actions):
                        axes[t].set_title("Action {}".format(t))
                        mp = axes[t].imshow(action_values[:, t].reshape(4, 4),
                                            interpolation='nearest', vmin=vmin,
                                            vmax=vmax)
                    fig.subplots_adjust(right=0.8)
                    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
                    fig.colorbar(mp, cax=cbar_ax)
                    plt.show()


class OptimalAgent(Agent):
    def select_action(self, policy, target_action_values, state):
        st = np.where(state)[1]
        if st == 0:
            return 1
        if st == 4:
            return 1
        if st == 8:
            return 2
        if st == 9:
            return 2
        if st == 10:
            return 1
        if st == 14:
            return 2
        return 0


class AgentSarsa(Agent):
    def update_network(self, memory, batch_size, hz):
        samples = memory.sample(batch_size)
        e_q_s_a = self.get_action_values(
            samples[2],
            True,
        )[np.arange(batch_size), samples[3]]
        target = samples[4] + self.gamma * e_q_s_a * samples[5]
        feed_dict = {
            self.q_input: samples[0],
            self.actions_onehot: self._onehot(samples[1], self.num_actions),
            self.target: target,
        }
        _ = self.sess.run(self.train_op, feed_dict)
        self.sess.run(self.target_update_ops)


class OptimalAgentSarsa(AgentSarsa):
    def select_action(self, policy, target_action_values, state):
        st = np.where(state)[1]
        if st == 0:
            return 1
        if st == 4:
            return 1
        if st == 8:
            return 2
        if st == 9:
            return 2
        if st == 10:
            return 1
        if st == 14:
            return 2
        return 0

