"""Core classes."""

from scipy.misc import imresize
import numpy as np
import attr


class ReplayMemory:
    def __init__(self, memory_size=1000000, state_size=(84,84,4)):
        self.states = np.empty((memory_size,) + state_size)
        self.actions = np.empty(memory_size, np.uint8)
        self.rewards = np.empty(memory_size)
        self.start_pos = np.empty(memory_size, np.int)

        self.last_start = 0
        self.last_idx = 0

        self.memory_size = memory_size
        self.memory_filled = False

    def append(self, state, action, reward, is_terminal):
        self.states[self.last_idx] = state.copy()
        self.actions[self.last_idx] = action
        self.rewards[self.last_idx] = reward
        self.start_pos[self.last_idx] = self.last_start

        self.last_idx += 1
        if self.last_idx == self.memory_size:
            self.last_idx = 0
            self.memory_filled = True

        if is_terminal:
            self.last_start = self.last_idx

    def sample(self, batch_size, hz):
        if self.memory_filled is True:
            upper_bound = self.memory_size
        else:
            upper_bound = self.last_idx
        idx = np.random.choice(upper_bound, batch_size, replace=False)
        is_valid = np.ones(idx.shape)

        if hz > 0:
            for i in range(batch_size):
                while idx[i] - hz < 0:
                    idx[i] = np.random.choice(upper_bound)
                if self.start_pos[idx[i] - hz] != self.start_pos[idx[i]]:
                    is_valid[i] = 0
            return (self.states[idx - hz], self.actions[idx - hz],
                    self.states[idx], self.actions[idx],
                    is_valid, self.rewards[idx])
        else:
            return self.states[idx], self.actions[idx], self.rewards[idx]

    def clear(self):
        self.memory_filled = False
        self.last_idx = 0
        self.last_start = 0

class ReplayMemoryWithTerminals(ReplayMemory):
    def sample(self, batch_size):
        if self.memory_filled is True:
            upper_bound = self.memory_size
        else:
            upper_bound = self.last_idx
        idx = np.random.choice(upper_bound, batch_size)
        non_terminals = np.ones(idx.shape, np.bool)
        hz = 1
        for i in range(batch_size):
            while idx[i] - hz < 0:
                idx[i] = np.random.choice(upper_bound)
            if self.start_pos[idx[i] - hz] != self.start_pos[idx[i]]:
                non_terminals[i] = False
        return (self.states[idx - hz], self.actions[idx - hz],
                self.states[idx], self.actions[idx], self.rewards[idx - hz],
                non_terminals)








