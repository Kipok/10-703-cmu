\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}

% \usepackage{nips_2016}

% to compile a camera-ready version, add the [final] option, e.g.:
\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{amsmath}
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{enumerate}
\usepackage{fancyvrb}
\usepackage{hyperref}
\usepackage{placeins}
\usepackage{tikz}
\usepackage{tikzsymbols}
\usepackage{todonotes}
\usepackage{adjustbox}

\title{Homework 1}

\author{
  Igor Gitman, igitman
}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Nm}{\mathcal{N}}
\newcommand{\Ll}{\mathcal{L}}

% brackets
\newcommand{\inner}[2]{\left\langle#1,#2 \right\rangle}
\newcommand{\rbr}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1\right\}}
\newcommand{\nbr}[1]{\left\|#1\right\|}
\newcommand{\abr}[1]{\left|#1\right|}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}

\maketitle
\section*{Problem 1}
\subsection*{Part a}
\begin{enumerate}[a)]
    \item The state is the position on a grid, so the number of states is $6\times6=36$.
    \item The number of actions is the number of possible moves $= 4$.
    \item Transition function $P$ is defined as $P : S \times S \times A \rightarrow [0, 1]$, so the dimensionality is $36 \times 36 \times 4 = 5184$.
    \item
        \begin{tabular}{|l|l|c|c|c|c|c|} \hline
             \multicolumn{2}{|c|}{} &
             \multicolumn{5}{|c|}{\textbf{s'}}\\\hline
             \textbf{s} & \textbf{a} & (1,2) & (1,1) & (1,4) & (1,3) & (5, 6)\\\hline
             (1, 1) & up & 1 & 0 & 0 & 0 & 0 \\ \hline
             (1, 1) & down & 0 & 1 & 0 & 0 & 0 \\ \hline
             (1, 3) & up & 0 & 0 & 0 & 1 & 0 \\ \hline
             (6, 6) & left & 0 & 0 & 0 & 0 & 0 \\ \hline
        \end{tabular}
    \item
        One possible reward function would be $R\rbr{(5,6), \text{right}, (6, 6)} = R\rbr{(6,5), \text{up}, (6,6)} = 1$, 
        $R\rbr{(6,6), a, (6, 6)} = 0\ \forall a$, $R\rbr{s, a, s'} = -1$ for all other $s, a, s'$.

        This way we will get reward of $+1$ only if we go to the coffee and reward of $-1$ doing all actions that don't lead to the coffee directly
        (which should force the optimal agent to go to the coffee as fast as possible). If we already reached the coffee, we will get zero reward for
        all other states. The discount coefficient value doesn't affect the optimal policy in this case since this is essentially an episodic problem.
    \item
        The value of the reward function doesn't matter since the optimal policy always reaches the terminal state in the finite number of steps.
        But the optimal value function will be scaled according to the choice of $\gamma$.
    \item
        The policy in this case is described by what action we choose at what state. Thus, the number of deterministic policies for this problem is
        $\cbr{\text{\# of actions}}^{\cbr{\text{\# of states}}} \approx 4 * 10^{21}$. If we consider stochastic policies as well, then they will form
        a continuum set.
    \item
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            	\includegraphics[width=0.5\textwidth]{opt.png}
          	}
		\end{minipage} 
    \item
        It is deterministic since all the actions are taken with probability $1$.
    \item
        There is no advantage to having a stochastic optimal policy in this case. However if we do exploration or in a real-world scenario when the
        conditions might change in time, stochastic policy might be more robust.
\end{enumerate}
\subsection*{Part b}
\begin{enumerate}[a)]
    \item
        \begin{tabular}{|l|l|c|c|c|} \hline
             \multicolumn{2}{|c|}{} &
             \multicolumn{3}{|c|}{\textbf{s'}}\\\hline
             \textbf{s} & \textbf{a} & (1,3) & (3,2) & (1,4)\\\hline
             (2, 2) & up & 0 & 0.1 & 0 \\ \hline
        \end{tabular}
    \item There are many of the optimal policies in the deterministic case. But in the stochastic case some of them will not be optimal anymore.
    Consider a position (5, 5). In the deterministic case both actions up and right are correct. But in the stochastic case the only correct action is
    to go right since in that case there is a $10\%$ chance of staying in the same place and $90\%$ chance of going in one of the right directions.
    But if we go up, there is a $90\%$ chance of going in the right direction and $10\%$ chance of going to the left which will bring us farther from
    the coffee then staying in the same place. Thus the correct action to choose is to go right.
    \item Yes, the value will change since on average the agent will do more steps to reach the coffee.
\end{enumerate}
\subsection*{Part c}
\begin{enumerate}[a)]
    \item Let's define reward function as follows: 
        \[ R(s, a, s') = R(s') = \begin{cases} -0.1\ \text{for the green states} \\ 1.0\ \text{for the coffee state} \\ 0\ \text{for the blue states} 
        \end{cases}\]
    \item No, the optimal policy will remain the same since it will be optimal to go to the coffee as soon as possible and recieve positive reward.
    Not going to coffee will lead to non-positive reward and not going as fast as possible will lower reward since it is negative for green states.
    \item $V_{\pi_a}\rbr{(1,5)} > V_{\pi_b}\rbr{(1,5)}$ since $V_{\pi_a}\rbr{(1,5)} = 0$ and $V_{\pi_b}\rbr{(1,5)} < 0$.
    \item It depends on the policies since both policies might try to go out of green region as soon as possible. Then if we look at $(1,5)$ state we
    have three cases: if $\pi_a(1,5) = $ up and $\pi_b(1,5) = $ down, then $V_{\pi_a}(s) \le V_{\pi_b}(s)\ \forall s$. If it is reverse situation then
    reverse inequality holds. If they both go up or both go down, then $V_{\pi_a}(s) = V_{\pi_b}(s)\ \forall s$.
\end{enumerate}
\section{Problem 2}
\subsection{Part a}
For Deterministic-4x4-FrozenLake-v0:
\begin{enumerate}[a)]
    \item Time taken for execution $= 2.11$ms, number of policy improvement steps $= 28$, number of policy evaluation steps $= 7$.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            \begin{BVerbatim}
                DRDL
                DLDL
                RDDL
                LRRL
            \end{BVerbatim}
          	}
		\end{minipage} 
    \item
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
				\includegraphics[width=0.5\textwidth]{val4.png}
          	}
		\end{minipage} 
    \item Time taken for execution $= 905\mu$s, number of iterations $= 7$.
    \item The function looks the same as in the policy iteration case.
    \item The algorithms took the same number of iterations, but value iteration was faster since it didn't do any intermediate policy evaluation
    steps.
    \item No, they are identical.
    \item This is the same policy as above.
    \item Yes, this value is the same as the one computed analytically: $0.59049$.
\end{enumerate}
For Deterministic-8x8-FrozenLake-v0:
\begin{enumerate}[a)]
    \item Time taken for execution $= 30.2$ms, number of policy improvement steps $= 120$, number of policy evaluation steps $= 15$.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            \begin{BVerbatim}
                DDDDDDDD
                DDDRDDDD
                DDDLDRDD
                RRRRDLDD
                RRULDDRD
                DLLRRDLD
                DLRULDLD
                RRULRRRL
            \end{BVerbatim}
          	}
		\end{minipage} 
    \item
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
				\includegraphics[width=0.5\textwidth]{val8.png}
          	}
		\end{minipage} 
    \item Time taken for execution $= 7.88$ms, number of iterations $= 15$.
    \item The function looks the same as in the policy iteration case.
    \item The algorithms took the same number of iterations, but value iteration was faster since it didn't do any intermediate policy evaluation
    steps.
    \item No, they are identical.
    \item This is the same policy as above.
    \item Yes, this value is the same as the one computed analytically: $\approx 0.2541866$.
\end{enumerate}
\subsection*{Part b}
For Stochastic-4x4-FrozenLake-v0:
\begin{enumerate}[a)]
    \item Time taken for execution $= 5$ms, number of iterations $= 23$.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
				\includegraphics[width=0.5\textwidth]{val4stoch.png}
          	}
		\end{minipage} 

        Yes, the value is different since the environment is now stochastic and we don't always go in the directions we try to go.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            \begin{BVerbatim}
                LULU
                LLLL
                UDLL
                LRDL
            \end{BVerbatim}
          	}
		\end{minipage} 

    \item Yes, the policy is different. For instance in the state $4$ the action chosen is to go left and not down, since the agent is avoiding a hole.
    \item No, the value doesn't match because, the value from value iteration function should be the correct expectation (but it is not, since the
    tolerance is very small in this case) and what I get from simulation is only an estimate. It matches up to the first few digits.
\end{enumerate}
For Stochastic-8x8-FrozenLake-v0:
\begin{enumerate}[a)]
    \item Time taken for execution $= 0.035$s, number of iterations $= 24$.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
				\includegraphics[width=0.5\textwidth]{val8stoch.png}
          	}
		\end{minipage} 

        Yes, the value is different since the environment is now stochastic and we don't always go in the directions we try to go.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            \begin{BVerbatim}
                DRRRRRRR
                URRURRRD
                URLLRURD
                UUUDLLRD
                UULLRDUR
                LLLDULLR
                LLDLLLLR
                RDLLDDDL
            \end{BVerbatim}
          	}
		\end{minipage} 

    \item Yes, the policy is different. For instance in the state $55$ the action chosen is to go right and not down, since the agent is avoiding 
    a hole.
    \item No, the value doesn't match because, the value from value iteration function should be the correct expectation (but it is not, since the
    tolerance is very small in this case) and what I get from simulation is only an estimate. It matches up to the first few digits.
\end{enumerate}
\subsection*{Part c}
\begin{enumerate}[a)]
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
				\includegraphics[width=0.5\textwidth]{neg4.png}
          	}
		\end{minipage}
    \item Yes, it is different since now it is meaningful for the agent to go to the holes if they are close enough.
    \item 
		\begin{minipage}[t]{\linewidth}
        	\raggedright
        	\adjustbox{valign=t}{%
            \begin{BVerbatim}
                DDLD
                RLLL
                DURL
                LLRL
            \end{BVerbatim}
          	}
		\end{minipage} 
    \item Yes, it is different, for instance in the state $4$ we go right so that to finish in the hole with zero 
    reward.
\end{enumerate}
\section*{Problem 4}
\begin{enumerate}[a)]
    \item $\mathcal{F}^*V^* = V^*$ is the definition of $V^*$. Let's prove that $\mathcal{F}^*V^* = V^*$ has a solution and uniqueness will follow
    from the part b. $\mathcal{F}^*V^* = V^* \Leftrightarrow V^* = \max_{a}\rbr{r(a) + \gamma P(a)V^*}$. Let's prove that 
    $v = r(a) + \gamma P(a)v \Leftrightarrow \rbr{I - \gamma P(a)}v = r(a)$ has a solution for all $a$. It's enough to prove that matrix 
    $\rbr{I - \gamma P(a)}$ is invertible or equivalently that it doesn't have a zero eigenvalue. Let's assume that it is not true:
    \[ \rbr{I - \gamma P(a)}v = 0 \Leftrightarrow \gamma P(a)v = Iv \Leftrightarrow P(a)v = \frac{1}{\gamma}v \]
    But $P(a)$ is a Markov matrix and thus it's biggest eigenvalue is $1$. And $\frac{1}{\gamma} > 1$ which is a contradiction.
    \item 
        \begin{align*}
            \nbr{\mathcal{F}^*(v) - \mathcal{F}^*(u)}_{\infty} &= \nbr{\max_a\sbr{r(a) + \gamma P(a)v} - \max_a\sbr{r(a) + \gamma P(a)u}}_{\infty} 
            \le \\
            &\le \nbr{\max_a\sbr{\gamma P(a)}\rbr{v - u}}_{\infty} \le \gamma\nbr{\rbr{v - u}}_{\infty} < \nbr{\rbr{v - u}}_{\infty}
        \end{align*}
        The second to last inequality holds since $\forall a$ rows of $P(a)$ sum up to $1$, thus:
        \[ 
        \nbr{P(a)x}_{\infty} = \max_k\sbr{\sum_{k=1}^nP(a)_{ki}x_i} \le \max_k\sbr{\sum_{k=1}^nP(a)_{ki}\max_i\rbr{x_i}} = \max_i\rbr{x_i}\ \forall a 
        \]
        Thus, by contraction mapping theorem $\rbr{\mathcal{F}^*}^kV_0$ converges to $V^*$ for any $V_0$.
    \item Given the optimal value function the optimal policy is just to follow the action on which maximum is achieved:
    \[ \pi^*(s) = \argmax_{a \in \mathcal{A}}\rbr{\sum_{s' \in \mathcal{S}}P(s'|s, a)\sbr{R(s, a, s') + \gamma V^*(s')}} \]
\end{enumerate}
\end{document}
