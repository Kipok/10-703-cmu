# coding: utf-8
from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import numpy as np

try:
    xrange
except NameError:
    xrange = range


def evaluate_policy(env, gamma, policy, max_iterations=int(1e3), tol=1e-3):
    """Evaluate the value of a policy.

    See page 87 (pg 105 pdf) of the Sutton and Barto Second Edition
    book.

    http://webdocs.cs.ualberta.ca/~sutton/book/bookdraft2016sep.pdf

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    policy: np.array
      The policy to evaluate. Maps states to actions.
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, int
      The value for the given policy and the number of iterations till
      the value function converged.
    """
    # assuming policy is deterministic
    V = np.zeros(env.nS)
    for iter_num in xrange(max_iterations):
        norm = 0
        for s in xrange(env.nS):
            cur_val = 0.0
            for p, s_next, r, is_term in env.P[s][policy[s]]:
                if is_term:
                    cur_val += p * r
                else:
                    cur_val += p * (r + gamma * V[s_next])
            norm = max(norm, np.abs(V[s] - cur_val))
            V[s] = cur_val
        if norm < tol:
            break
   #     print("iter", iter_num, " ", V.reshape(4,4))
    return V, iter_num + 1


def value_function_to_policy(env, gamma, value_function):
    """Output action numbers for each state in value_function.

    Parameters
    ----------
    env: gym.core.Environment
      Environment to compute policy for. Must have nS, nA, and P as
      attributes.
    gamma: float
      Discount factor. Number in range [0, 1)
    value_function: np.ndarray
      Value of each state.

    Returns
    -------
    np.ndarray
      An array of integers. Each integer is the optimal action to take
      in that state according to the environment dynamics and the
      given value function.
    """
    policy = np.zeros(env.nS, dtype='int')
    for s in xrange(env.nS):
        max_val = -np.inf
        for a in xrange(env.nA):
            cur_val = 0
            for p, s_next, r, is_term in env.P[s][a]:
                if is_term:
                    cur_val += p * r
                else:
                    cur_val += p * (r + gamma * value_function[s_next])
            if cur_val > max_val:
                policy[s] = a
                max_val = cur_val
    return policy


def improve_policy(env, gamma, value_func, policy):
    """Given a policy and value function improve the policy.

    See page 87 (pg 105 pdf) of the Sutton and Barto Second Edition
    book.

    http://webdocs.cs.ualberta.ca/~sutton/book/bookdraft2016sep.pdf

        Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    value_func: np.ndarray
      Value function for the given policy.
    policy: dict or np.array
      The policy to improve. Maps states to actions.

    Returns
    -------
    bool, np.ndarray
      Returns true if policy changed. Also returns the new policy.
    """
    policy_new = value_function_to_policy(env, gamma, value_func)
    return np.array_equal(policy_new, policy), policy_new


def policy_iteration(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs policy iteration.

    See page 87 (pg 105 pdf) of the Sutton and Barto Second Edition
    book.

    http://webdocs.cs.ualberta.ca/~sutton/book/bookdraft2016sep.pdf

    You should use the improve_policy and evaluate_policy methods to
    implement this method.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    (np.ndarray, np.ndarray, int, int)
       Returns optimal policy, value function, number of policy
       improvement iterations, and number of value iterations.
    """
    policy = np.zeros(env.nS, dtype='int')
    pol_num = 0
    V = np.zeros(env.nS)
    for iter_num in xrange(max_iterations):
        V, cur_num = evaluate_policy(env, gamma, policy, tol=tol)
        pol_num += cur_num
        is_same, new_policy = improve_policy(env, gamma, V, policy)
        if is_same:
            break
        policy = new_policy
    #    print(V.reshape(4, 4))
    #    print(policy.reshape(4, 4))

    return policy, V, pol_num, iter_num + 1


def value_iteration(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs value iteration for a given gamma and environment.

    See page 90 (pg 108 pdf) of the Sutton and Barto Second Edition
    book.

    http://webdocs.cs.ualberta.ca/~sutton/book/bookdraft2016sep.pdf

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, iteration
      The value function and the number of iterations it took to converge.
    """
    V = np.zeros(env.nS)
    for iter_num in xrange(max_iterations):
        norm = 0
        for s in xrange(env.nS):
            max_val = -np.inf
            for a in xrange(env.nA):
                cur_val = 0
                for p, s_next, r, is_term in env.P[s][a]:
                    if is_term:
                        cur_val += p * r
                    else:
                        cur_val += p * (r + gamma * V[s_next])
                max_val = max(max_val, cur_val)
            norm = max(norm, np.abs(V[s] - max_val))
            V[s] = max_val
        if norm < tol:
            break
     #   print("iter,", iter_num, " ", V.reshape(4,4))
    return V, iter_num + 1


def print_policy(policy, action_names):
    """Print the policy in human-readable format.

    Parameters
    ----------
    policy: np.ndarray
      Array of state to action number mappings
    action_names: dict
      Mapping of action numbers to characters representing the action.
    """
    str_policy = policy.astype('str')
    for action_num, action_name in action_names.items():
        np.place(str_policy, policy == action_num, action_name)

    print(str_policy)
