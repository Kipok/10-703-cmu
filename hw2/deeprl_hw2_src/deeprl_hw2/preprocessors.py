import numpy as np
from scipy.misc import imresize


class Preprocessor:
    """ All proprocessing in this class
    """

    def __init__(self, history_length=3, out_shape=(84, 84)):
        self.history_length = history_length
        self.out_shape = out_shape
        self.history_frames = np.zeros(out_shape + (history_length,))

    def add_history(self, frame):
        state = np.dstack((self.history_frames, frame))
        self.history_frames[:,:,:self.history_length - 1] = \
                self.history_frames[:,:,1:self.history_length]
        self.history_frames[:,:,-1] = frame
        return state

    def reset(self):
        """Reset the history sequence.
        Useful when you start a new episode.
        """
        self.history_frames.fill(0)

    def _rgb2y(self, img):
        return (0.299 * img[:,:,0] +
                0.587 * img[:,:,1] +
                0.114 * img[:,:,2]).round().astype(np.uint8)

    def process_frame(self, frame):
        """Scale, convert to greyscale and store as uint8.

        We don't want to save floating point numbers in the replay
        memory. We get the same resolution as uint8, but use a quarter
        to an eigth of the bytes (depending on float32 or float64)
        """
        # TODO: look at the maximum?
        return imresize(self._rgb2y(frame), self.out_shape)

    def to_network(self, frames):
        return frames / 127.5 - 1.0

    def process_with_history(self, frame):
        return self.add_history(self.process_frame(frame))

    def process_reward(self, reward):
        """Clip reward between -1 and 1."""
        if reward > 1.0:
            return 1.0
        if reward < -1.0:
            return -1.0
        return reward

