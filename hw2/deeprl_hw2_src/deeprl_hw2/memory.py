"""Core classes."""

from scipy.misc import imresize
import numpy as np
import attr


class ReplayMemory:
    """Interface for replay memories.

    We have found this to be a useful interface for the replay
    memory. Feel free to add, modify or delete methods/attributes to
    this class.

    It is expected that the replay memory has implemented the
    __iter__, __getitem__, and __len__ methods.

    If you are storing raw Sample objects in your memory, then you may
    not need the end_episode method, and you may want to tweak the
    append method. This will make the sample method easy to implement
    (just randomly draw samples saved in your memory).

    However, the above approach will waste a lot of memory (as states
    will be stored multiple times in s as next state and then s' as
    state, etc.). Depending on your machine resources you may want to
    implement a version that stores samples in a more memory efficient
    manner.

    Methods
    -------
    append(state, action, reward, debug_info=None)
      Add a sample to the replay memory. The sample can be any python
      object, but it is suggested that tensorflow_rl.core.Sample be
      used.
    end_episode(final_state, is_terminal, debug_info=None)
      Set the final state of an episode and mark whether it was a true
      terminal state (i.e. the env returned is_terminal=True), of it
      is is an artificial terminal state (i.e. agent quit the episode
      early, but agent could have kept running episode).
    sample(batch_size, indexes=None)
      Return list of samples from the memory. Each class will
      implement a different method of choosing the
      samples. Optionally, specify the sample indexes manually.
    clear()
      Reset the memory. Deletes all references to the samples.
    """
    def __init__(self, memory_size=1000000, state_size=(84,84,4)):
        """Setup memory.

        5 separate numpy arrays are kept: for states, actions, rewards,
        next_states, and is_terminal.
        """
        # TODO: optimize this!!
        self.states = np.empty((memory_size,) + state_size, np.uint8)
        self.actions = np.empty(memory_size, np.uint8)
        self.rewards = np.empty(memory_size)
        self.next_states = np.empty((memory_size,) + state_size, np.uint8)
        self.not_terminal = np.empty(memory_size, np.bool)
        self.last_idx = 0
        self.memory_size = memory_size
        self.memory_filled = False

    def append(self, state, action, reward, next_state, is_terminal):
        self.states[self.last_idx] = state.copy()
        self.actions[self.last_idx] = action
        self.rewards[self.last_idx] = reward
        self.next_states[self.last_idx] = next_state
        self.not_terminal[self.last_idx] = np.invert(is_terminal)
        self.last_idx += 1
        if self.last_idx == self.memory_size:
            self.last_idx = 0
            self.memory_filled = True

    def sample(self, batch_size, use_last=False):
        if self.memory_filled is True:
            idx = np.random.choice(self.memory_size, batch_size)
        else:
            idx = np.random.choice(self.last_idx, batch_size)

        if use_last is True:
            idx = np.arange(self.last_idx - batch_size, self.last_idx)
            if self.memory_filled is True:
                idx[idx < 0] += self.memory_size
            else:
                idx[idx < 0] = 0

        return Samples(self.states[idx], self.actions[idx],
                self.rewards[idx], self.next_states[idx],
                self.not_terminal[idx])

    def clear(self):
        self.memory_filled = False
        self.last_idx = 0

@attr.s
class Samples:
    states = attr.ib()
    actions = attr.ib()
    rewards = attr.ib()
    next_states = attr.ib()
    not_terminals = attr.ib()

