"""Main DQN agent."""

import attr
from keras.models import Sequential
import tensorflow as tf
import numpy as np
from .policy import LinearDecayGreedyEpsilonPolicy, UniformRandomPolicy, \
        GreedyEpsilonPolicy
from .objectives import mean_huber_loss
import os
import gym
from gym import wrappers


class EnvWrapper:
    def __init__(self, env, repeat_num=4):
        self.env = env
        self.repeat_num = repeat_num
        self.last_frame = np.zeros(env.observation_space.shape)

    def reset(self):
        return self.env.reset()

    def step(self, action):
        cum_reward = 0
        for i in range(self.repeat_num - 1):
            next_frame, reward, is_terminal, other_info = self.env.step(action)
            cum_reward += reward
            if is_terminal:
                maxed_frame = np.maximum(next_frame, self.last_frame)
                self.last_frame = next_frame
                return maxed_frame, cum_reward, is_terminal, other_info

        maxed_frame = np.maximum(next_frame, self.last_frame)
        self.last_frame = next_frame
        return maxed_frame, cum_reward, is_terminal, other_info

    def render(self, close=False):
        return self.env.render(close=close)

    def close(self):
        self.env.close()


@attr.s
class DQNAgent:
    """Class implementing DQN.
    Parameters
    ----------
    q_network: keras.models.Model
      Your Q-network model.
    preprocessor: deeprl_hw2.core.Preprocessor
      The preprocessor class. See the associated classes for more
      details.
    memory: deeprl_hw2.core.Memory
      Your replay memory.
    gamma: float
      Discount factor.
    target_update_freq: float
      Frequency to update the target network. You can either provide a
      number representing a soft target update (see utils.py) or a
      hard target update (see utils.py and Atari paper.)
    num_burn_in: int
      Before you begin updating the Q-network your replay memory has
      to be filled up with some number of samples. This number says
      how many.
    train_freq: int
      How often you actually update your Q-Network. Sometimes
      stability is improved if you collect a couple samples for your
      replay memory, for every Q-network update that you run.
    batch_size: int
      How many samples in each minibatch.
    """
    sess = attr.ib()
    q_network = attr.ib()
    preprocessor = attr.ib()
    memory = attr.ib()
    action_size = attr.ib()
    log_dir = attr.ib()
    model_dir = attr.ib()
    gamma = attr.ib(0.99)
    target_update_freq = attr.ib(10000)
    num_burn_in = attr.ib(50000)
    train_freq = attr.ib(4)
    batch_size = attr.ib(32)
    output_info_freq = attr.ib(10000)
    num_steps_decay = attr.ib(1000000)
    update_last = attr.ib(False)

    def compile(self, loss_func=None, optimizer=None):
        if loss_func is None:
            loss_func = mean_huber_loss
        if optimizer is None:
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
        # define target network
        with tf.name_scope('target_network'):
            self.target_network = Sequential.from_config(
                self.q_network.get_config())
        self.target_update_ops = [
            tf.assign(w1, w2) for w1, w2 in
            zip(self.target_network.weights, self.q_network.weights)
        ]
        # define state placeholders
        self.actions_onehot = tf.placeholder(tf.float32,
                                             [None, self.action_size],
                                             name='actions_onehot')
        self.rewards = tf.placeholder(tf.float32, [None], name='rewards')
        self.not_terminals = tf.placeholder(tf.float32,
                                            [None], name='not_terminals')
        # define target value
        self.target = self.gamma * tf.reduce_max(self.target_network.output,
                                                 axis=1) * \
                      self.not_terminals + self.rewards
        # compute q value for a given action and define the loss op
        self.q_action_val = tf.reduce_sum(self.q_network.output *
                                          self.actions_onehot, axis=1)
        self.loss = loss_func(self.target, self.q_action_val)
        # define optimize operation only using q_network weights
        self.train_op = optimizer.minimize(self.loss,
                                           var_list=self.q_network.weights)

        # define some dummy variables for logging in tensorboard
        self.iter_ph = tf.placeholder(tf.int32, name='iter_num_ph')
        self.iter = tf.Variable(0, dtype=tf.int32, name='iter_num')
        self.assign_iter = tf.assign(self.iter, self.iter_ph)

        # define starting policy and initialize variables
        self.policy = UniformRandomPolicy(self.action_size)
        self.sess.run(tf.global_variables_initializer())
        # start with networks synchronized
        self.sess.run(self.target_update_ops)

    def calc_q_values(self, states):
        """Given a state (or batch of states) calculate the Q-values.

        Basically run your network on these states.

        Return
        ------
        Q-values for the state(s)
        """
        states_to_network = self.preprocessor.to_network(states)
        return self.sess.run(self.q_network.output,
                             {self.q_network.input: states_to_network})

    def select_action(self, state):
        """Select the action based on the current state.

        You will probably want to vary your behavior here based on
        which stage of training your in. For example, if you're still
        collecting random samples you might want to use a
        UniformRandomPolicy.

        If you're testing, you might want to use a GreedyEpsilonPolicy
        with a low epsilon.

        If you're training, you might want to use the
        LinearDecayGreedyEpsilonPolicy.

        This would also be a good place to call
        process_state_for_network in your preprocessor.

        Returns
        --------
        selected action
        """
        action = self.policy.select_action(
            q_values=self.calc_q_values(np.expand_dims(state, 0)))
        return action

    def _onehot(self, x, num):
        out = np.zeros((x.size, num))
        out[np.arange(x.size), x] = 1.0
        return out

    def update_networks(self, iter_num, use_last=False):
        """Update your policy.

        Behavior may differ based on what stage of training your
        in. If you're in training mode then you should check if you
        should update your network parameters based on the current
        step and the value you set for train_freq.

        Inside, you'll want to sample a minibatch, calculate the
        target values, update your network, and then update your
        target values.

        You might want to return the loss and other metrics as an
        output. They can help you monitor how training is going.
        """
        # sampling minibatch and running one Adam step
        samples = self.memory.sample(self.batch_size, use_last=use_last)
        feed_dict = {
            self.q_network.input: self.preprocessor.to_network(samples.states),
            self.target_network.input: self.preprocessor.to_network(
                samples.next_states),
            self.actions_onehot: self._onehot(
                samples.actions, self.action_size),
            self.rewards: samples.rewards,
            self.not_terminals: samples.not_terminals,
        }
        _, loss = self.sess.run([self.train_op, self.loss], feed_dict)

        # updating target network if necessary
        if iter_num % self.target_update_freq == 0:
            self.sess.run(self.target_update_ops)

        return loss

    def fit(self, env, num_iterations, model_file=None,
            max_episode_length=None, no_monitor=False):
        """Fit your model to the provided environment.

        Its a good idea to print out things like loss, average reward,
        Q-values, etc to see if your agent is actually improving.

        You should probably also periodically save your network
        weights and any other useful info.

        This is where you should sample actions from your network,
        collect experience samples and add them to your replay memory,
        and update your network parameters.

        Parameters
        ----------
        env: gym.Env
          This is your Atari environment.
        num_iterations: int
          How many samples/updates to perform.
        max_episode_length: int
          How long a single episode should last before the agent
          resets. Can help exploration.
        """
        log_writer = tf.summary.FileWriter(self.log_dir, self.sess.graph)
        saver = tf.train.Saver()
        start_iter = 0
        if model_file is not None:
            saver.restore(self.sess, model_file)
            start_iter = self.sess.run(self.iter)

        episode_iter = 0
        total_loss = 0

        state = self.reset_and_get_first_state(env)
        # dummy states in the beginning
        self.states_to_check = np.zeros((200,) + state.shape)

        for iter_num in range(start_iter, num_iterations):
            # getting next action and loading state into memory
            action = self.select_action(state)
            next_frame, reward, is_terminal, other_info = env.step(action)
            reward = self.preprocessor.process_reward(reward)
            next_state = self.preprocessor.process_with_history(next_frame)
            self.memory.append(state, action, reward, next_state, is_terminal)
            state = next_state

            # checking if it's time to stop using uniform policy
            if iter_num - start_iter  == self.num_burn_in:
                print("Stopped pure exploration")
                self.policy = LinearDecayGreedyEpsilonPolicy(
                    self.action_size, num_steps=self.num_steps_decay)
                # fixing random states to check q-values during training
                self.states_to_check = self.memory.sample(200).states

            # updating network parameters
            if iter_num - start_iter >= self.num_burn_in and \
               iter_num % self.train_freq == 0:
                total_loss += self.update_networks(iter_num, self.update_last)

            episode_iter += 1
            # if episode is finished, reset parameters
            if episode_iter == max_episode_length or is_terminal:
                episode_iter = 0
                state = self.reset_and_get_first_state(env)
            if no_monitor is False:
                # every once in a while output training info and save the model
                if iter_num % self.output_info_freq == 0:
                    q_values = self.calc_q_values(self.states_to_check)
                    avg_q_values = np.mean(np.max(q_values, axis=1))
                    avg_loss = total_loss / self.output_info_freq
                    avg_reward, reward_std, avg_length = self.evaluate(env, 20)
                    print(("iter #{}: loss={:.5f}, " +
                           "reward={:.2f}, length={:.0f} q-values={:.4f}")
                          .format(iter_num, avg_loss, avg_reward,
                                  avg_length, avg_q_values))
                    # saving training info in tensorboard
                    summary = tf.Summary(value=[
                        tf.Summary.Value(tag='average loss',
                                         simple_value=avg_loss),
                        tf.Summary.Value(tag='average reward',
                                         simple_value=avg_reward),
                        tf.Summary.Value(tag='average q values',
                                         simple_value=avg_q_values),
                        tf.Summary.Value(tag='average length',
                                         simple_value=avg_length),
                    ])
                    log_writer.add_summary(summary, iter_num)
                    log_writer.flush()
                    self.sess.run(self.assign_iter, {self.iter_ph: iter_num})
                    saver.save(
                        self.sess,
                        os.path.join(self.model_dir, 'model'),
                        iter_num
                    )
                    total_loss = 0
                    state = self.reset_and_get_first_state(env)
                # generating videos
                if iter_num == 0:
                    print("Generating first video")
                    self._generate_monitor(env, 0)
                if iter_num == num_iterations // 3:
                    print("Generating second video")
                    self._generate_monitor(env, 1)
                if iter_num == 2 * num_iterations // 3:
                    print("Generating third video")
                    self._generate_monitor(env, 2)
                if iter_num == num_iterations - 1:
                    print("Generating fourth video")
                    self._generate_monitor(env, 3)
        log_writer.close()

    def _generate_monitor(self, env, fraction):
        mon_env = EnvWrapper(wrappers.Monitor(
            env.env,
            os.path.join(self.log_dir, 'monitor{}-3'.format(fraction))
        ))
        self.evaluate(mon_env, 1)
        mon_env.close()

    def reset_and_get_first_state(self, env):
        self.preprocessor.reset()
        return self.preprocessor.process_with_history(env.reset())

    def evaluate(self, env, num_episodes, verbose=False,
                 model_file=None, max_episode_length=None):
        """Test your agent with a provided environment.

        You shouldn't update your network parameters here. Also if you
        have any layers that vary in behavior between train/test time
        (such as dropout or batch norm), you should set them to test.

        Basically run your policy on the environment and collect stats
        like cumulative reward, average episode length, etc.

        You can also call the render function here if you want to
        visually inspect your policy.
        """
        saver = tf.train.Saver()
        if model_file is not None:
            saver.restore(self.sess, model_file)

        policy = GreedyEpsilonPolicy(0.05, self.action_size)
        state = self.reset_and_get_first_state(env)
        rewards = []
        episode_reward = 0
        total_episode_length = 0
        episode_iter = 0
        cur_episode_length = 0
        while episode_iter < num_episodes:
            if verbose:
                env.render()
            action = policy.select_action(
                q_values=self.calc_q_values(np.expand_dims(state, 0))
            )
            next_frame, reward, is_terminal, other_info = env.step(action)
            episode_reward += reward
            state = self.preprocessor.process_with_history(next_frame)
            cur_episode_length += 1
            if is_terminal or cur_episode_length == max_episode_length:
                episode_iter += 1
                if episode_iter != num_episodes:
                    state = self.reset_and_get_first_state(env)
                total_episode_length += cur_episode_length
                cur_episode_length = 0
                rewards.append(episode_reward)
                episode_reward = 0
        if verbose:
            print("avg episode reward: {:.2f}+-{:.2f}, avg episode length: {:.2f}".format(
                np.mean(rewards), np.std(rewards), total_episode_length / num_episodes))
        return np.mean(rewards), np.std(rewards), total_episode_length / num_episodes

class DQNAgentNoReplayNoFixing(DQNAgent):
    def compile(self, loss_func=None, optimizer=None):
        if loss_func is None:
            loss_func = mean_huber_loss
        if optimizer is None:
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
        # define state placeholders
        self.target_input = tf.placeholder(
            tf.float32,
            self.q_network.input_shape,
            name='target_input')
        self.actions_onehot = tf.placeholder(tf.float32,
                                             [None, self.action_size],
                                             name='actions_onehot')
        self.rewards = tf.placeholder(tf.float32, [None], name='rewards')
        self.not_terminals = tf.placeholder(tf.float32,
                                            [None], name='not_terminals')
        # define target value
        self.target = self.gamma * \
                tf.reduce_max(self.q_network(self.target_input), axis=1) * \
                self.not_terminals + self.rewards
        # compute q value for a given action and define the loss op
        self.q_action_val = tf.reduce_sum(self.q_network.get_output_at(0) *
                                          self.actions_onehot, axis=1)
        self.loss = loss_func(self.target, self.q_action_val)
        # define optimize operation only using q_network weights
        self.train_op = optimizer.minimize(self.loss,
                                           var_list=self.q_network.weights)

        # define some dummy variables for logging in tensorboard
        self.iter_ph = tf.placeholder(tf.int32, name='iter_num_ph')
        self.iter = tf.Variable(0, dtype=tf.int32, name='iter_num')
        self.assign_iter = tf.assign(self.iter, self.iter_ph)
        with tf.name_scope('training_info'):
            tf.summary.histogram('q_network_w', self.q_network.weights[0])
            tf.summary.histogram('q_network_b', self.q_network.weights[1])
            self.all_summaries = tf.summary.merge_all()

        # define starting policy and initialize variables
        self.policy = UniformRandomPolicy(self.action_size)
        self.sess.run(tf.global_variables_initializer())

        self.train_freq = self.batch_size

    def calc_q_values(self, states):
        """Given a state (or batch of states) calculate the Q-values.

        Basically run your network on these states.

        Return
        ------
        Q-values for the state(s)
        """
        states_to_network = self.preprocessor.to_network(states)
        return self.sess.run(self.q_network.get_output_at(0),
                             {self.q_network.get_input_at(0):
                              states_to_network})

    def update_networks(self, iter_num, use_last):
        """Update your policy.

        Behavior may differ based on what stage of training your
        in. If you're in training mode then you should check if you
        should update your network parameters based on the current
        step and the value you set for train_freq.

        Inside, you'll want to sample a minibatch, calculate the
        target values, update your network, and then update your
        target values.

        You might want to return the loss and other metrics as an
        output. They can help you monitor how training is going.
        """
        # sampling minibatch and running one Adam step
        samples = self.memory.sample(self.batch_size, use_last=True)
        feed_dict = {
            self.q_network.get_input_at(0):
            self.preprocessor.to_network(samples.states),
            self.target_input: self.preprocessor.to_network(
                samples.next_states),
            self.actions_onehot: self._onehot(
                samples.actions, self.action_size),
            self.rewards: samples.rewards,
            self.not_terminals: samples.not_terminals,
        }
        _, loss = self.sess.run([self.train_op, self.loss], feed_dict)

        return loss


class DQNAgentDoubleLearning(DQNAgent):
    def compile(self, loss_func=None, optimizer=None):
        if loss_func is None:
            loss_func = mean_huber_loss
        if optimizer is None:
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
        # define target network
        with tf.name_scope('target_network'):
            self.target_network = Sequential.from_config(
                self.q_network.get_config())
        self.target_update_ops = [
            tf.assign(w1, w2) for w1, w2 in
            zip(self.target_network.weights, self.q_network.weights)
        ]
        # define state placeholders
        self.actions_onehot = tf.placeholder(tf.float32,
                                             [None, self.action_size],
                                             name='actions_onehot')
        self.rewards = tf.placeholder(tf.float32, [None], name='rewards')
        self.not_terminals = tf.placeholder(tf.float32,
                                            [None], name='not_terminals')
        # define target value
        target_argmax_q = tf.gather_nd(
            self.target_network.output,
            tf.transpose(
                tf.stack([tf.range(0, tf.shape(self.q_network.output)[0]),
                          tf.cast(tf.argmax(self.q_network.output, -1),
                                  tf.int32)])
            )
        )
        self.target = self.gamma * target_argmax_q * \
                      self.not_terminals + self.rewards
        # compute q value for a given action and define the loss op
        self.q_action_val = tf.reduce_sum(self.q_network.output *
                                          self.actions_onehot, axis=1)
        self.loss = loss_func(self.target, self.q_action_val)
        # define optimize operation only using q_network weights
        self.train_op = optimizer.minimize(self.loss,
                                           var_list=self.q_network.weights)

        # define some dummy variables for logging in tensorboard
        self.iter_ph = tf.placeholder(tf.int32, name='iter_num_ph')
        self.iter = tf.Variable(0, dtype=tf.int32, name='iter_num')
        self.assign_iter = tf.assign(self.iter, self.iter_ph)

        # define starting policy and initialize variables
        self.policy = UniformRandomPolicy(self.action_size)
        self.sess.run(tf.global_variables_initializer())
        # start with networks synchronized
        self.sess.run(self.target_update_ops)

class DQNAgentDueling(DQNAgent):
    def compile(self, q_network_output, q_network_input,
                target_network_output, target_network_input,
                loss_func=None, optimizer=None):
        if loss_func is None:
            loss_func = mean_huber_loss
        if optimizer is None:
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
        self.q_network_output = q_network_output
        self.q_network_input = q_network_input
        self.target_network_output = target_network_output
        self.target_network_input = target_network_input
        # define target network
        target_weights = [var for var in tf.trainable_variables()
                          if var.name.startswith('target')]
        q_weights = [var for var in tf.trainable_variables()
                     if var.name.startswith('q_network')]
        self.target_update_ops = [
            tf.assign(w1, w2) for w1, w2 in
            zip(target_weights, q_weights)
        ]
        # define state placeholders
        self.actions_onehot = tf.placeholder(tf.float32,
                                             [None, self.action_size],
                                             name='actions_onehot')
        self.rewards = tf.placeholder(tf.float32, [None], name='rewards')
        self.not_terminals = tf.placeholder(tf.float32,
                                            [None], name='not_terminals')
        # define target value
        target_argmax_q = tf.gather_nd(
            self.target_network_output,
            tf.transpose(
                tf.stack([tf.range(0, tf.shape(self.q_network_output)[0]),
                          tf.cast(tf.argmax(self.q_network_output, -1),
                                  tf.int32)])
            )
        )
        self.target = self.gamma * target_argmax_q * \
                      self.not_terminals + self.rewards
        # compute q value for a given action and define the loss op
        self.q_action_val = tf.reduce_sum(self.q_network_output *
                                          self.actions_onehot, axis=1)
        self.loss = loss_func(self.target, self.q_action_val)
        # define optimize operation only using q_network weights
        self.train_op = optimizer.minimize(self.loss,
                                           var_list=q_weights)

        # define some dummy variables for logging in tensorboard
        self.iter_ph = tf.placeholder(tf.int32, name='iter_num_ph')
        self.iter = tf.Variable(0, dtype=tf.int32, name='iter_num')
        self.assign_iter = tf.assign(self.iter, self.iter_ph)

        # define starting policy and initialize variables
        self.policy = UniformRandomPolicy(self.action_size)
        self.sess.run(tf.global_variables_initializer())
        # start with networks synchronized
        self.sess.run(self.target_update_ops)

    def calc_q_values(self, states):
        """Given a state (or batch of states) calculate the Q-values.

        Basically run your network on these states.

        Return
        ------
        Q-values for the state(s)
        """
        states_to_network = self.preprocessor.to_network(states)
        return self.sess.run(self.q_network_output,
                             {self.q_network_input: states_to_network})

    def update_networks(self, iter_num, use_last=False):
        """Update your policy.

        Behavior may differ based on what stage of training your
        in. If you're in training mode then you should check if you
        should update your network parameters based on the current
        step and the value you set for train_freq.

        Inside, you'll want to sample a minibatch, calculate the
        target values, update your network, and then update your
        target values.

        You might want to return the loss and other metrics as an
        output. They can help you monitor how training is going.
        """
        # sampling minibatch and running one Adam step
        samples = self.memory.sample(self.batch_size, use_last=use_last)
        feed_dict = {
            self.q_network_input: self.preprocessor.to_network(samples.states),
            self.target_network_input: self.preprocessor.to_network(
                samples.next_states),
            self.actions_onehot: self._onehot(
                samples.actions, self.action_size),
            self.rewards: samples.rewards,
            self.not_terminals: samples.not_terminals,
        }
        _, loss = self.sess.run([self.train_op, self.loss], feed_dict)

        # updating target network if necessary
        if iter_num % self.target_update_freq == 0:
            self.sess.run(self.target_update_ops)

        return loss

