"""Run Atari Environment with DQN."""
import argparse
import os
import random
import gym

import numpy as np
import tensorflow as tf
from keras.layers import (Activation, Convolution2D, Dense,
                          Flatten, Input, Permute)
from keras.models import Model, Sequential

from deeprl_hw2.dqn import DQNAgent, DQNAgentNoReplayNoFixing, \
        DQNAgentDoubleLearning, DQNAgentDueling
from deeprl_hw2.preprocessors import Preprocessor
from deeprl_hw2.memory import ReplayMemory
from deeprl_hw2.objectives import mean_huber_loss, mean_squared_loss
from deeprl_hw2.dqn import EnvWrapper


def create_model(window, input_shape, num_actions,
                 model_name='linear_model', name_scope='q_network'):
    """Create the Q-network model.

    Use Keras to construct a keras.models.Model instance (you can also
    use the SequentialModel class).

    We highly recommend that you use tf.name_scope as discussed in
    class when creating the model and the layers. This will make it
    far easier to understand your network architecture if you are
    logging with tensorboard.

    Parameters
    ----------
    window: int
      Each input to the network is a sequence of frames. This value
      defines how many frames are in the sequence.
    input_shape: tuple(int, int)
      The expected input image size.
    num_actions: int
      Number of possible actions. Defined by the gym environment.
    model_name: str
      Useful when debugging. Makes the model show up nicer in tensorboard.

    Returns
    -------
    keras.models.Model
      The Q-model.
    """
    if model_name == 'linear_model':
        with tf.name_scope('q_network'):
            model = Sequential()
            model.add(Flatten(
                input_shape=(input_shape[0], input_shape[1], window))
            )
            model.add(Dense(num_actions))
    if model_name == 'deep_model':
        with tf.name_scope('q_network'):
            model = Sequential()
            model.add(Convolution2D(
                32, (8, 8), strides=(4, 4),
                input_shape=(input_shape[0], input_shape[1], window))
            )
            model.add(Activation('relu'))
            model.add(Convolution2D(64, (4, 4), strides=(2, 2)))
            model.add(Activation('relu'))
            model.add(Convolution2D(64, (3, 3)))
            model.add(Activation('relu'))
            model.add(Flatten())
            model.add(Dense(512))
            model.add(Activation('relu'))
            model.add(Dense(num_actions))
    if model_name == 'dueling_model':
        with tf.name_scope(name_scope):
            model = Sequential()
            model.add(Convolution2D(
                32, (8, 8), strides=(4, 4),
                input_shape=(input_shape[0], input_shape[1], window))
            )
            model.add(Activation('relu'))
            model.add(Convolution2D(64, (4, 4), strides=(2, 2)))
            model.add(Activation('relu'))
            model.add(Convolution2D(64, (3, 3)))
            model.add(Activation('relu'))
            model.add(Flatten())
            value_layer = Dense(512, activation='relu')(model.output)
            value_output = Dense(1)(value_layer)
            advantage_layer = Dense(512, activation='relu')(model.output)
            advantage_output = Dense(num_actions)(advantage_layer)
            model_output = value_output + advantage_output - \
                    tf.reduce_mean(advantage_output, axis=-1, keep_dims=True)
            return model_output, model.input
    return model


def get_output_folder(parent_dir, env_name, create_new=True):
    """Return save folder.

    Assumes folders in the parent_dir have suffix -run{run
    number}. Finds the highest run number and sets the output folder
    to that number + 1. This is just convenient so that if you run the
    same script multiple times tensorboard can plot all of the results
    on the same plots with different names.

    Parameters
    ----------
    parent_dir: str
      Path of the directory containing all experiment runs.

    Returns
    -------
    parent_dir/run_dir
      Path to this run's save directory.
    """
    os.makedirs(parent_dir, exist_ok=True)
    experiment_id = 0
    for folder_name in os.listdir(parent_dir):
        if not os.path.isdir(os.path.join(parent_dir, folder_name)):
            continue
        try:
            folder_name = int(folder_name.split('-run')[-1])
            if folder_name > experiment_id:
                experiment_id = folder_name
        except:
            pass
    if create_new is True:
        experiment_id += 1

    parent_dir = os.path.join(parent_dir, env_name)
    parent_dir = parent_dir + '-run{}'.format(experiment_id)
    os.makedirs(parent_dir, exist_ok=True)
    return parent_dir


def main():
    parser = argparse.ArgumentParser(description='Run DQN on Atari game')
    parser.add_argument('--env', default='SpaceInvadersNoFrameskip-v3',
                        help='Atari env name')
    parser.add_argument('--seed', default=0, type=int, help='Random seed')
    parser.add_argument('--question', type=int)

    args = parser.parse_args()

    tf.set_random_seed(args.seed)
    np.random.seed(args.seed)

    env = gym.make(args.env)
    if args.question == 7:
        log_dir = get_output_folder('exps-q7', args.env)
        model_dir = get_output_folder('models-q7', args.env)
        q_network_output, q_network_input = create_model(4, (84, 84), env.action_space.n,
                                                     'dueling_model', 'q_network')
        target_network_output, target_network_input = create_model(4, (84, 84), env.action_space.n,
                                                               'dueling_model', 'target_network')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgentDueling(
                sess, 'dummy', preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(q_network_output, q_network_input,
                          target_network_output, target_network_input,
                          mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

    if args.question == 6:
        log_dir = get_output_folder('exps-q6', args.env)
        model_dir = get_output_folder('models-q6', args.env)
        q_network = create_model(4, (84, 84), env.action_space.n, 'deep_model')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgentDoubleLearning(
                sess, q_network, preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

    if args.question == 5:
        log_dir = get_output_folder('exps-q5', args.env)
        model_dir = get_output_folder('models-q5', args.env)
        q_network = create_model(4, (84, 84), env.action_space.n, 'deep_model')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgent(
                sess, q_network, preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

    if args.question == 4:
        log_dir = get_output_folder('exps-q4', args.env)
        model_dir = get_output_folder('models-q4', args.env)
        q_network = create_model(4, (84, 84), env.action_space.n, 'linear_model')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgentDoubleLearning(
                sess, q_network, preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

    if args.question == 3:
        log_dir = get_output_folder('exps-q3', args.env)
        model_dir = get_output_folder('models-q3', args.env)
        q_network = create_model(4, (84, 84), env.action_space.n, 'linear_model')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgent(
                sess, q_network, preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

    if args.question == 2:
        log_dir = get_output_folder('exps-q2', args.env)
        model_dir = get_output_folder('models-q2', args.env)
        q_network = create_model(4, (84, 84), env.action_space.n, 'linear_model')
        preprocessor = Preprocessor()
        memory = ReplayMemory()
        with tf.Session() as sess:
            agent = DQNAgentNoReplayNoFixing(
                sess, q_network, preprocessor, memory,
                env.action_space.n, log_dir, model_dir,
            )
            agent.compile(mean_huber_loss)
            agent.fit(EnvWrapper(env), 5000000)
            reward, reward_std, length = agent.evaluate(EnvWrapper(env), 100)
            print("Final evaluation: {}, {}".format(reward, reward_std))

if __name__ == '__main__':
    main()

