import unittest
import numpy as np
import numpy.testing as npt
import tensorflow as tf
import gym
import os
import shutil
from keras.models import Sequential

from deeprl_hw2.memory import ReplayMemory
from deeprl_hw2.preprocessors import Preprocessor
from deeprl_hw2.policy import UniformRandomPolicy
from deeprl_hw2.policy import GreedyPolicy
from deeprl_hw2.policy import GreedyEpsilonPolicy
from deeprl_hw2.policy import LinearDecayGreedyEpsilonPolicy
from deeprl_hw2.objectives import mean_squared_loss, mean_huber_loss, huber_loss
from deeprl_hw2.dqn import DQNAgent, DQNAgentNoReplayNoFixing, \
        DQNAgentDoubleLearning
from dqn_atari import create_model


class TestReplayMemory(unittest.TestCase):
    def setUp(self):
        self.sz = 20
        self.memory = ReplayMemory(memory_size=self.sz + 10)

        self.states, self.actions, self.rewards, \
                self.next_states, self.is_terminals = self.gen_random_samples(self.sz)
        for i in range(self.sz):
            self.memory.append(self.states[i], self.actions[i], self.rewards[i],
                               self.next_states[i], self.is_terminals[i])

    def gen_random_samples(self, sz):
        return np.random.randint(0, 256, (sz, 84, 84, 4)), np.random.choice(10, sz), \
               np.random.rand(sz), np.random.randint(0, 256, (sz, 84, 84, 4)), \
               np.random.rand(sz) > 0.5

    def compare_samples_arrays(self, s1, a1, r1, ns1, nt1, s2, a2, r2, ns2, nt2):
        return np.array_equal(s1, s2) and np.array_equal(a1, a2) and \
               np.array_equal(r1, r2) and np.array_equal(ns1, ns2) and \
               np.array_equal(nt1, nt2)

    def compare_samples_arrays_assert(self, s1, a1, r1, ns1, nt1, s2, a2, r2, ns2, nt2):
        npt.assert_array_almost_equal(s1, s2)
        npt.assert_array_almost_equal(a1, a2)
        npt.assert_array_almost_equal(r1, r2)
        npt.assert_array_almost_equal(ns1, ns2)
        npt.assert_array_almost_equal(nt1, nt2)

    def test_correct_elements_and_size(self):
        batch_sizes = [1, 5, self.sz * 2]
        for batch_size in batch_sizes:
            samples = self.memory.sample(batch_size)
            self.assertEqual(samples.states.shape[0], batch_size)
            self.assertEqual(samples.actions.shape[0], batch_size)
            self.assertEqual(samples.rewards.shape[0], batch_size)
            self.assertEqual(samples.next_states.shape[0], batch_size)
            self.assertEqual(samples.not_terminals.shape[0], batch_size)
            for i in range(batch_size):
                flag = False
                for j in range(self.sz):
                    equals =  self.compare_samples_arrays(
                        samples.states[i],
                        samples.actions[i],
                        samples.rewards[i],
                        samples.next_states[i],
                        not samples.not_terminals[i],
                        self.states[j],
                        self.actions[j],
                        self.rewards[j],
                        self.next_states[j],
                        self.is_terminals[j]
                    )
                    if equals:
                        flag = True
                        break
                self.assertTrue(flag)

    def test_memory_fill(self):
        sz = 20
        states, actions, rewards, next_states, is_terminals = self.gen_random_samples(sz)
        for i in range(sz):
            self.memory.append(states[i], actions[i], rewards[i],
                               next_states[i], is_terminals[i])
        # TODO: kind of bad to assume it stores them..
        self.compare_samples_arrays_assert(
            self.memory.states[:10],
            self.memory.actions[:10],
            self.memory.rewards[:10],
            self.memory.next_states[:10],
            self.memory.not_terminal[:10],
            states[10:],
            actions[10:],
            rewards[10:],
            next_states[10:],
            (is_terminals == False)[10:]
        )
        self.compare_samples_arrays_assert(
            self.memory.states[-10:],
            self.memory.actions[-10:],
            self.memory.rewards[-10:],
            self.memory.next_states[-10:],
            self.memory.not_terminal[-10:],
            states[:10],
            actions[:10],
            rewards[:10],
            next_states[:10],
            (is_terminals == False)[:10]
        )


    def test_use_last(self):
        def test_size(sz):
            states, actions, rewards, next_states, is_terminals = self.gen_random_samples(sz)
            for i in range(sz):
                self.memory.append(states[i], actions[i], rewards[i],
                                   next_states[i], is_terminals[i])
            samples = self.memory.sample(sz, use_last=True)
            self.compare_samples_arrays_assert(
                samples.states, samples.actions, samples.rewards,
                samples.next_states, samples.not_terminals,
                states, actions, rewards, next_states, is_terminals == False
            )
        test_size(5)
        test_size(1)
        test_size(20)

    def test_use_last_more_than_have(self):
        self.memory.clear()
        states, actions, rewards, next_states, is_terminals = self.gen_random_samples(5)
        for i in range(5):
            self.memory.append(states[i], actions[i], rewards[i],
                               next_states[i], is_terminals[i])
        samples = self.memory.sample(15, use_last=True)
        self.compare_samples_arrays_assert(
            samples.states[-5:], samples.actions[-5:], samples.rewards[-5:],
            samples.next_states[-5:], samples.not_terminals[-5:],
            states, actions, rewards, next_states, is_terminals == False
        )
        self.compare_samples_arrays_assert(
            samples.states[:-5], samples.actions[:-5], samples.rewards[:-5],
            samples.next_states[:-5], samples.not_terminals[:-5],
            np.repeat(np.expand_dims(states[0], 0), 10, 0),
            np.repeat(np.expand_dims(actions[0], 0), 10, 0),
            np.repeat(np.expand_dims(rewards[0], 0), 10, 0),
            np.repeat(np.expand_dims(next_states[0], 0), 10, 0),
            np.repeat(np.expand_dims(is_terminals[0] == False, 0), 10, 0)
        )

    def test_memory_clear(self):
        self.memory.clear()
        self.assertFalse(self.memory.memory_filled)
        self.assertEqual(self.memory.last_idx, 0)

class TestPreprocessor(unittest.TestCase):
    def setUp(self):
        self.preprocessor = Preprocessor()

    def test_process_reward(self):
        self.assertEqual(self.preprocessor.process_reward(5), 1.0)
        self.assertEqual(self.preprocessor.process_reward(1.0), 1.0)
        self.assertEqual(self.preprocessor.process_reward(0.5), 0.5)
        self.assertEqual(self.preprocessor.process_reward(0), 0.0)
        self.assertEqual(self.preprocessor.process_reward(-0.1), -0.1)
        self.assertEqual(self.preprocessor.process_reward(-10.1), -1.0)

    def test_to_network(self):
        frames = np.random.randint(0, 256, (10, 84, 84))
        processed_frames = self.preprocessor.to_network(frames)
        npt.assert_almost_equal(processed_frames.mean(), 0, 2)
        npt.assert_almost_equal(processed_frames.max(), 1.0, 2)
        npt.assert_almost_equal(processed_frames.min(), -1.0, 2)
        self.assertLessEqual(processed_frames.max(), 1.0)
        self.assertLessEqual(-1.0, processed_frames.min())

    def test_process_frame(self):
        frame = np.random.randint(0, 256, (160, 230, 3))
        processed_frame = self.preprocessor.process_frame(frame)
        self.assertEqual(processed_frame.shape, self.preprocessor.out_shape)
        self.assertEqual(processed_frame.dtype, np.uint8)
        self.assertTrue(np.any(processed_frame))

    def test_process_with_history(self):
        frame = np.random.randint(0, 256, (160, 230, 3))
        first = self.preprocessor.process_with_history(frame)
        self.preprocessor.reset()
        second = self.preprocessor.add_history(
            self.preprocessor.process_frame(frame))
        npt.assert_almost_equal(first, second)

    def test_reset(self):
        for _ in range(10):
            self.preprocessor.add_history(np.random.randint(1, 256, (84, 84)))
        self.assertTrue(np.all(self.preprocessor.history_frames))
        self.preprocessor.reset()
        self.assertFalse(np.any(self.preprocessor.history_frames))

    def test_add_history(self):
        self.assertFalse(np.any(self.preprocessor.history_frames))
        for i in range(self.preprocessor.history_length):
            frame = np.random.randint(0, 256, (84, 84))
            history = self.preprocessor.add_history(frame)
            self.assertFalse(np.any(history[:,:,:-i-1]))
        frames = np.random.randint(0, 256, (10, 84, 84))
        for i in range(10):
            history = self.preprocessor.add_history(frames[i])
            if i <= self.preprocessor.history_length:
                npt.assert_array_almost_equal(
                    np.rollaxis(history[:,:,self.preprocessor.history_length - i:], 2),
                    frames[:i + 1]
                )
            else:
                npt.assert_array_almost_equal(
                    np.rollaxis(history, 2),
                    frames[i - self.preprocessor.history_length:i + 1]
                )

class TestPolicies(unittest.TestCase):
    def test_uniform_policy(self):
        num_actions = 10
        policy = UniformRandomPolicy(num_actions)
        actions = np.zeros(num_actions)
        sz = 100000
        for i in range(sz):
            actions[policy.select_action(q_values=np.array([1,2,3]))] += 1

        actions /= sz
        npt.assert_array_almost_equal(actions, np.ones(num_actions) / num_actions, 2)

    def test_greedy_policy(self):
        policy = GreedyPolicy()
        for num_actions in [1, 10, 50, 100]:
            q_values = np.random.rand(num_actions)
            self.assertEqual(policy.select_action(q_values=q_values), np.argmax(q_values))

    def test_epsilon_greedy_policy(self):
        for num_actions in [4, 10]:
            for epsilon in [0.001, 0.1, 0.9, 1.0]:
                num_actions = 10
                epsilon = 0.001
                q_values = np.random.rand(num_actions)
                q_values[0] = 10
                policy = GreedyEpsilonPolicy(epsilon, num_actions)
                actions = np.zeros(num_actions)
                sz = 10000
                for i in range(sz):
                    actions[policy.select_action(q_values=q_values)] += 1
                actions /= sz
                npt.assert_almost_equal(actions.sum(), 1.0)
                npt.assert_array_almost_equal(
                    actions[1:],
                    np.ones(num_actions - 1) / num_actions * epsilon, 2
                )

    def test_linear_decay_policy(self):
        num_actions = 10
        policy = LinearDecayGreedyEpsilonPolicy(num_actions=num_actions, num_steps=5)
        lspace = np.linspace(policy.start_value, policy.end_value, policy.num_steps + 1)
        for i in range(policy.num_steps):
            npt.assert_almost_equal(policy.cur_eps, lspace[i])
            q_values = np.random.rand(num_actions)
            actions1 = np.zeros(num_actions)
            actions2 = np.zeros(num_actions)
            policy2 = GreedyEpsilonPolicy(policy.cur_eps, num_actions)
            sz = 20000
            for j in range(sz):
                actions1[policy.select_action(q_values, is_training=False)] += 1
                actions2[policy2.select_action(q_values)] += 1
            policy.select_action(q_values)
            npt.assert_array_almost_equal(actions1 / sz, actions2 / sz, 2)
        for i in range(10):
            policy.select_action(q_values)
            npt.assert_almost_equal(policy.cur_eps, policy.end_value)


class TestObjectives(unittest.TestCase):
    def setUp(self):
        self.sess = tf.Session()
        self.var1 = tf.placeholder(tf.float32)
        self.var2 = tf.placeholder(tf.float32)

    def tearDown(self):
        self.sess.close()

    def test_mean_squared_loss(self):
        loss = mean_squared_loss(self.var1, self.var2)
        grads = tf.gradients(loss, [self.var1, self.var2])
        def numpy_loss_grads(y_true, y_pred):
            loss = np.mean((y_true - y_pred) ** 2)
            grads = [2 * (y_true - y_pred), 2 * (y_pred - y_true)]
            return loss, grads

        var1_val = np.random.rand()
        var2_val = np.random.rand()
        feed_dict = {self.var1: var1_val, self.var2: var2_val}
        tf_loss, tf_grads = self.sess.run([loss, grads], feed_dict)
        np_loss, np_grads = numpy_loss_grads(var1_val, var2_val)

        npt.assert_almost_equal(np_loss, tf_loss)
        npt.assert_array_almost_equal(np_grads, tf_grads)

    def test_huber_loss(self):
        loss = huber_loss(self.var1, self.var2)
        grads = tf.gradients(loss, [self.var1, self.var2])
        def numpy_loss_grads(y_true, y_pred, max_grad=1.0):
            loss = 0.5 * (y_true - y_pred) ** 2
            abs_val = np.abs(y_true - y_pred)
            loss_ln = max_grad * abs_val - 0.5 * max_grad ** 2
            loss[np.where(abs_val > max_grad)] = loss_ln[np.where(abs_val > max_grad)]
            grads = [y_true - y_pred, y_pred - y_true]
            grads[0][np.where(y_true - y_pred < -max_grad)] = -max_grad
            grads[0][np.where(y_true - y_pred > max_grad)] = max_grad
            grads[1][np.where(y_pred - y_true < -max_grad)] = -max_grad
            grads[1][np.where(y_pred - y_true > max_grad)] = max_grad
            return loss, grads

        var1_val = np.random.rand(100) * 5
        var2_val = np.random.rand(100) * 5
        feed_dict = {self.var1: var1_val, self.var2: var2_val}
        tf_loss, tf_grads = self.sess.run([loss, grads], feed_dict)
        np_loss, np_grads = numpy_loss_grads(var1_val, var2_val)

        npt.assert_array_almost_equal(np_loss, tf_loss)
        npt.assert_array_almost_equal(np_grads[0], tf_grads[0])
        npt.assert_array_almost_equal(np_grads[1], tf_grads[1])

    def test_mean_huber_loss(self):
        loss = mean_huber_loss(self.var1, self.var2)
        grads = tf.gradients(loss, [self.var1, self.var2])
        def numpy_loss_grads(y_true, y_pred, max_grad=1.0):
            loss = 0.5 * (y_true - y_pred) ** 2
            abs_val = np.abs(y_true - y_pred)
            loss_ln = max_grad * abs_val - 0.5 * max_grad ** 2
            loss[np.where(abs_val > max_grad)] = loss_ln[np.where(abs_val > max_grad)]
            grads = [y_true - y_pred, y_pred - y_true]
            grads[0][np.where(y_true - y_pred < -max_grad)] = -max_grad
            grads[0][np.where(y_true - y_pred > max_grad)] = max_grad
            grads[1][np.where(y_pred - y_true < -max_grad)] = -max_grad
            grads[1][np.where(y_pred - y_true > max_grad)] = max_grad
            grads[0] /= loss.size
            grads[1] /= loss.size
            return np.mean(loss), grads

        var1_val = np.random.rand(100) * 5
        var2_val = np.random.rand(100) * 5
        feed_dict = {self.var1: var1_val, self.var2: var2_val}
        tf_loss, tf_grads = self.sess.run([loss, grads], feed_dict)
        np_loss, np_grads = numpy_loss_grads(var1_val, var2_val)

        npt.assert_almost_equal(np_loss, tf_loss)
        npt.assert_array_almost_equal(np_grads, tf_grads)


class EnvTest:
    def __init__(self):
        self.bad_state = np.random.randint(0, 256, (84, 84, 3))
        self.normal_state = np.random.randint(0, 256, (84, 84, 3))
        self.good_state = np.random.randint(0, 256, (84, 84, 3))

        self.states = [self.bad_state, self.normal_state, self.good_state]
        self.rewards = [-0.1, 0, 0.1]
        self.cur_state = 0
        self.num_iters = 0
        self.was_in_second = False

    def reset(self):
        self.cur_state = 0
        self.num_iters = 0
        self.was_in_second = False
        return self.states[self.cur_state]

    def step(self, action):
        assert(0 <= action <= 3)
        self.num_iters += 1
        if action < 3:
            self.cur_state = action
        reward = self.rewards[self.cur_state]
        if self.was_in_second is True:
            reward *= -10
        if self.cur_state == 1:
            self.was_in_second = True
        else:
            self.was_in_second = False
        return self.states[self.cur_state], reward, \
                self.num_iters >= 5, {}


class TestAgent(unittest.TestCase):
    def setUp(self):
        tf.set_random_seed(42)
        np.random.seed(42)

        env_name = 'SpaceInvaders-v0'
        self.env = gym.make(env_name)
        os.makedirs('tmp_exps')
        os.makedirs('tmp_models')
        self.q_network = create_model(
            4, (84, 84),
            self.env.action_space.n, 'linear_model'
        )
        self.preprocessor = Preprocessor()
        self.memory = ReplayMemory()
        self.sess = tf.Session()
        self.dqn_agent = DQNAgent

    def tearDown(self):
        self.sess.close()
        shutil.rmtree('tmp_exps')
        shutil.rmtree('tmp_models')

    def test_network_target_updates(self):
        agent = self.dqn_agent(
            self.sess, self.q_network, self.preprocessor,
            self.memory, self.env.action_space.n, 'tmp_exps',
            'tmp_models', output_info_freq=25, num_burn_in=1,
            target_update_freq=5, train_freq=1)
        agent.compile(mean_huber_loss)

        q_weights = agent.sess.run(agent.q_network.weights)
        t_weights = agent.sess.run(agent.target_network.weights)
        for w1, w2 in zip(q_weights, t_weights):
            npt.assert_almost_equal(w1, w2)

        agent.fit(self.env, 3, no_monitor=True)
        q_weights1 = agent.sess.run(agent.q_network.weights)
        t_weights1 = agent.sess.run(agent.target_network.weights)
        for w1, w2 in zip(t_weights, t_weights1):
            npt.assert_almost_equal(w1, w2)

        for w1, w2 in zip(q_weights1, t_weights):
            self.assertTrue(np.any(np.not_equal(w1, w2)))

        agent.fit(self.env, 6, no_monitor=True)
        q_weights = agent.sess.run(agent.q_network.weights)
        t_weights = agent.sess.run(agent.target_network.weights)
        for w1, w2 in zip(q_weights, t_weights):
            npt.assert_almost_equal(w1, w2)

    def test_network_converge_target_fixed(self):
        num_actions = 5
        num_samples = 20
        for i in range(num_samples):
            self.memory.append(
                np.random.rand(84, 84, 4) * 10,
                np.random.randint(0, num_actions),
                (np.random.rand() - 0.5) * 10,
                np.random.rand(84, 84, 4) * 10, np.random.rand() > 0.5
            )

        agent = self.dqn_agent(
            self.sess, self.q_network, self.preprocessor,
            self.memory, self.env.action_space.n, 'tmp_exps',
            'tmp_models', output_info_freq=25, num_burn_in=1,
            target_update_freq=1000000, train_freq=1,
            batch_size=num_samples
        )
        agent.compile(mean_squared_loss)
        for i in range(5000):
            loss = agent.update_networks(1, True)

        loss = agent.update_networks(1, True)
        npt.assert_almost_equal(loss, 0)

    def test_network_solves_easy_env(self):
        env_name = 'test'
        env = EnvTest()

        q_network = create_model(4, (84, 84), 4, 'linear_model')
        agent = self.dqn_agent(
            self.sess, q_network, self.preprocessor,
            self.memory, 4, 'tmp_exps',
            'tmp_models', output_info_freq=25, num_burn_in=5,
            target_update_freq=100, train_freq=1, batch_size=1
        )
        agent.compile(
            mean_huber_loss,
            tf.train.GradientDescentOptimizer(learning_rate=0.00001)
        )
        agent.fit(env, 10000, no_monitor=True)
        reward, length = agent.evaluate(env, 50)
        self.assertLess(1.9, reward)


class TestDQNAgentNoReplayNoFixing(TestAgent):
    def setUp(self):
        tf.set_random_seed(42)
        np.random.seed(42)

        env_name = 'SpaceInvaders-v0'
        self.env = gym.make(env_name)
        os.makedirs('tmp_exps')
        os.makedirs('tmp_models')
        self.q_network = create_model(
            4, (84, 84),
            self.env.action_space.n, 'linear_model'
        )
        self.preprocessor = Preprocessor()
        self.memory = ReplayMemory()
        self.sess = tf.Session()
        self.dqn_agent = DQNAgentNoReplayNoFixing

    def test_network_target_updates(self):
        pass

    def test_network_loss_tf_numpy(self):
        pass

    def test_network_converge_target_fixed(self):
        pass


class TestDQNAgentDoubleLearning(TestAgent):
    def setUp(self):
        tf.set_random_seed(42)
        np.random.seed(42)

        env_name = 'SpaceInvaders-v0'
        self.env = gym.make(env_name)
        os.makedirs('tmp_exps')
        os.makedirs('tmp_models')
        self.q_network = create_model(
            4, (84, 84),
            self.env.action_space.n, 'linear_model'
        )
        self.preprocessor = Preprocessor()
        self.memory = ReplayMemory()
        self.sess = tf.Session()
        self.dqn_agent = DQNAgentDoubleLearning

    def test_network_target_updates(self):
        pass

    def test_network_loss_tf_numpy(self):
        pass

    def test_network_converge_target_fixed(self):
        pass

if __name__ == '__main__':
    unittest.main()
